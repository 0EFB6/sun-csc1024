def useless():

    print(""" ____                                                   _          _
|  _ \ ___  ___ ___  _ __ ___  _ __ ___   ___ _ __   __| | ___  __| |
| |_) / _ \/ __/ _ \| '_ ` _ \| '_ ` _ \ / _ \ '_ \ / _` |/ _ \/ _` |
|  _ <  __/ (_| (_) | | | | | | | | | | |  __/ | | | (_| |  __/ (_| |
|_| \_\___|\___\___/|_| |_| |_|_| |_| |_|\___|_| |_|\__,_|\___|\__,_|""", end="")
    print("""____  _     _
|  _ \(_)___| |__   ___  ___
| | | | / __| '_ \ / _ \/ __|
| |_| | \__ \ | | |  __/\__ \\
|____/|_|___/_| |_|\___||___/
""")

original_list = [['Slot 4', 'HAHA LAO', 'hahalao@gmail.com', '0166666333', '3'], ['Slot 1', 'LOL LAO', 'lol@gmail.com', '0123456789', '1']]

# Create a new list for extracted data
extracted_list = []

# Iterate over the original list
for item in original_list:
    if item[0].startswith('Slot '):  # Check if the first element starts with 'Slot '
        extracted_list.append(item)

# Print the extracted list
print(extracted_list)

extracted_list[0][0] = "Slot HAHA"

print(original_list)
print(extracted_list)
original_list = [['Slot 1', 'MICHELLE LIM', 'michellelim@gmail.com', '0134275689', '3'], ['Slot 2', 'LINDA WONG', 'lindawong@gmail.com', '0156743298', '2'], ['Slot 3', 'AMY TAN', 'amytan@gmail.com', '0194632857', '2'], ['Slot 4', 'STEVEN NG', 'stevenng@gmail.com', '0168457023', '3']]

new_list = [sublist[:] for sublist in original_list if "Slot" in sublist[0]]
new_list[0][0] = "HAHA"
print(new_list)
print(original_list)

available_slots = {
        "Slot 1": 2,
        "Slot 2": 2,
        "Slot 3": 2,
        "Slot 4": 2
    }

for i in available_slots:
    print(type(available_slots.get(i)))
