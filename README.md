# sun-csc1024

Final assignment written in Python for the subject CSC1024 in BCNS course.

## Progress - Report
The project's report has not been completed yet.

- [ ] Flowchart
- [ ] Discussion of the programming techniques used
- [x] Sample test cases and outputs
- [x] Python Code (copied from source file)
- [x] Sample text file
- [x] Task List

Additional tasks:

- [x] Code Comments
- [x] Identifier Names

## Progress - Code
The code part for the project is completed.

- [x] Optimie code
- [x] Organize code
- [x] Fix bug
- [x] Each session can accodomate 8 reservations, change it! 
- [x] Add checking for names to prevent multiple reservation made by a single person
- [x] Review & Improve Functions Call
- [x] Fix date validation to only allow user input XXXX-XX-XX date, not XXXX-X-X

Required functions:

- [x] Main menu
- [x] Add Reservation(s)
- [x] Cancel Reservation(s)
- [x] Update Reservation(s)
- [x] Display Reservations
- [x] Dish Recommendation
- [x] Exit Function

## Known Issues

Ticks indicate issue has been resolved.

- [x] [HIGH] Cancellation with '0' during slot selection after entering date in update_reservation() function is broken
- [x] [HIGH] Cancel function is broken inside update_reservation() function
- [x] [HIGH] Multiple updates for the date in update_reservation() function can be very buggy
- [x] [HIGH] Changes that have been made before user confirmation should not be made to the main reservation list inside update_reservation() - SUGGESTION: To use a temporary list instead of modifying it directly
- [x] [HIGH] Display will show out empty reservation for certain date that are added using Add Reservation function but user canceled the operation
- [x] [HIGH] Date and slot number may conflict during reser
- [x] [LOW] Changing date during update_reservation will not allow user to change to an earlier date
- [x] [LOW] Long names or long emails may affect the formatting when displaying
- [x] [LOW] Display function will not sort the reservations according to the slot number when the reservations are newly added