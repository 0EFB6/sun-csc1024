# CSC 1024 Python Final Assignment

import random
from menu import *
from utils import *
from add_reservation import *
from datetime import datetime, timedelta

############################################
################# MAIN MENU ################
############################################
def menu():
    reservation_list = organizeList(get_date_list(read_from_reservation("reservation_23026149.txt")), read_from_reservation("reservation_23026149.txt"))

    while True:
        clear_screen()
        print("Charming Thyme Trattoria Restaurant Management System")
        print("\n[1] Add Reservation(s)")
        print("[2] Cancel Reservation(s)")
        print("[3] Update/Edit Reservation(s)")
        print("[4] Display")
        print("[5] Generate Meal Recommendation")
        print("[6] Exit")
        print("-" * 36)
        print("| Enter a number (1-6) to continue |")
        print("-" * 36)
        
        option = get_menu_option()

        if option == 1:
            add_reservation(reservation_list)
        elif option == 2:
            cancel_reservation(reservation_list)
        elif option == 3:
            update_reservation(reservation_list)
        elif option == 4:
            display(reservation_list)
            press_enter_to_continue()
        elif option == 5:
            generate_recommendation()
            press_enter_to_continue()
        elif option == 6:
            write_reservation_data_to_file(reservation_list, "reservation_23026149.txt")
            clear_screen()
            exit("Goodbye!")

############################################
################### NO 1 ###################
############################################
def add_reservation(reservation_list):
    trigger = True
    date_list = get_date_list(reservation_list)

    # Get the date of the reservation and validate the date
    while True:
        if trigger:
            clear_screen()
        try:
            user_input = input("Enter the reservation date (YYYY-MM-DD): ").strip()
            current_date = datetime.now().date()
            target_date = current_date + timedelta(days=8)
            input_date = datetime.strptime(user_input, "%Y-%m-%d").date()

            if input_date < target_date:
                print("#" * 78)
                print(f"##  The date you entered is invalid. Please enter a date after {(target_date - timedelta(days=1)).strftime('%Y-%m-%d')}.  ##")
                print("#" * 78, "\n")
                trigger = False
                continue

            date = input_date.strftime("%Y-%m-%d")
            
            # Search through the data list to find the date, if exist, append to the list, if not, create a new nested list
            if date not in date_list:
                date_list.append(date)
                reservation_list.append([date])

            reservation = reservation_list[date_list.index(date)]
            #slot_number = len(reservation_list[date_list.index(date)]) - 1
            if len(reservation) - 1 >= 8:
                print("#" * 80)
                print(f"##  There are no available slots for {date}. Please choose another date.  ##")
                print("#" * 80, "\n")
            else:
                slots = reservation[1:]
                slot1, slot2, slot3, slot4 = check_available_slots(slots)
                slot = get_slot(slot1, slot2, slot3, slot4)
                information_list = [
                    slot,
                    get_name(),
                    get_email(),
                    get_phone_number(),
                    get_pax()
                ]
                clear_screen()
                display_reservation_information(information_list, date, False)
                if confirm_add_reservation():
                    reservation.append(information_list)
                    slot = information_list[0].replace("'", "").strip()
                    clear_screen()
                    print("#" * 74)
                    print(f"##   Reservation for {slot} on {date} has been made successfully!   ##")
                    print("#" * 74)
                else:
                    clear_screen()
                    print("#" * 31)
                    print("##  Add reservation failed.  ##")
                    print("#" * 31)                
                if not prompt_switch():
                    break
                else:
                    trigger = True
        except ValueError:
            print("#" * 74)
            print("##  Invalid date format. Please enter a date in the YYYY-MM-DD format.  ##")
            print("#" * 74, "\n")
            trigger = False

############################################
################### NO 2 ###################
############################################
def cancel_reservation(reservation_list):
    
    while True:
        clear_screen()
        # Get the date of the reservation and validate the date
        input_date = get_date_input_to_cancel(reservation_list)
        clear_screen()
        for reservation in reservation_list:
            if input_date == reservation[0]:
                number_of_slots = display_cancel_reservation(reservation)
                slot = get_slot_to_cancel(number_of_slots)
                clear_screen()
                cancel_slot(input_date, slot, reservation_list)
        if not prompt_switch():
            break
        
############################################
################### NO 3 ###################
############################################
def update_reservation(reservation_list):
    while True:
        clear_screen()
        input_date = get_date_input_to_update(reservation_list)
        clear_screen()

        for reservation in reservation_list:
            if input_date == reservation[0]:
                reservation_slot_list = reservation[1:]
                number_of_slots = display_update_reservation(reservation)
                slot = get_slot_to_update(number_of_slots)
                clear_screen()
                update(slot, reservation, reservation_slot_list, reservation_list)
        
        if not prompt_switch():
            break
        #except ValueError:
        #    print("Invalid date format. Please enter a date in the YYYY-MM-DD format.")
        #    trigger = False

############################################
################### NO 4 ###################
############################################
def display(reservation_list):
    # Clear the screen in terminal
    clear_screen()
    
    reservation_list.sort()
    # Print the reservations
    for dateReservations in reservation_list:
        date = dateReservations[0]
        reservations = dateReservations[1:]
        print("-" * 81)
        print("|" + " " * 34 + f"{date}" + " " * 35 + "|")
        print("-" * 81)
        for information in reservations:
            print(f"| {information[0]:<6} | {information[1]:<19} | {information[2]:<28} | {information[3]:<11} | {information[4]} |")
        print("-" * 81)
        print("\n")

############################################
################### NO 5 ###################
############################################
        
def generate_recommendation():
    menuList = read_from_menu();
    clear_screen()
    print(" " * 20, "#" * 26)
    print(" " * 20, "##  Recommended dishes  ##")
    print(" " * 20, "#" * 26, "\n")
    print("     -------------------------------------------------------------      ")
    print("    /                                                             \     ")
    print(f"   |       {random.choice(menuList):<50}      |    ")
    print("    \                                                             /     ")
    print("     -------------------------------------------------------------      ")
    print()

############################################
############################################
############################################       
def write_reservation_data_to_file(reservation_list, file_name="reservation.txt"):
    with open(file_name, "w") as file:
        for entry in reservation_list:
            date, *slots = entry
            for slot in slots:
                slot_info = '|'.join([date] + slot)
                file.write(slot_info + '\n')

menu()
