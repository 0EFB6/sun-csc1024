import os
from add_reservation import check_available_slots, get_slot, get_name, get_email, get_phone_number, get_pax

def clear_screen():
     os.system("cls" if os.name == "nt" else "clear")
     print()
     
def read_from_menu():
    menu_list = []
    with open("menuItems_23026149.txt", "r") as file:
        menus = file.readlines()
        for i in range(len(menus)):
            menu_list.append( menus[i].strip("\n"))
    return menu_list

def read_from_reservation(file_name="reservation.txt"):
    reservationList = []
    with open(file_name, "r") as file:
        reservations = file.readlines()
        for reservation in reservations:
            reservationList.append(reservation.strip("\n").split("|"))
    reservationList.sort()
    return reservationList

def get_date_list(reservationList):
    # Create a list to store dates
    dateList = []
    for reservation in reservationList:
        date = reservation[0]
        dateList.append(date) if date not in dateList else None
    dateList.sort()

    # Return the date list
    return dateList

def organizeList(dateList, reservationList):
    # Create a list of lists to store reservations by date
    organizeList = []
    for date in dateList:
        dateReservations = [date]
        for reservation in reservationList:
            if reservation[0] == date:
                dateReservations.append(reservation[1:])
        organizeList.append(dateReservations)
    organizeList.sort()
    return organizeList

def press_enter_to_continue():
	input("Press Enter to continue...")

def prompt_switch():
    while True:
        print()
        try:            
            print("Enter 'Y' to continue perform current operation or 'N' to return to main menu")
            switch = input("Your choice: ").strip().upper()
            if switch == "Y":
                return True
            elif switch == "N":
                return False
            print("Invalid choice.")
        except ValueError:
            print("Invalid choice. Please enter 'Y' or 'N'.")














############################################
############ CANCEL RESERVATION ############
############################################
def display_cancel_reservation(reservation_list):
    reservations = reservation_list[1:]

    print("-" * 83)
    print("|" + " " * 27 + f"Reservations for {reservation_list[0]}" + " " * 27 + "|")
    print("-" * 83)
    print(f"| Slot No. | Name               | Email                       | Phone No.   | Pax |")
    print("-" * 83)

    for i, reservation in enumerate(reservations, start=1):
        slot, name, email, phone, pax = reservation
        print(f"[{i:<1}] {slot:<6} | {name:<18} | {email:<27} | {phone:<11} | {pax:^3} |")
    print("-" * 83, "\n")

    return len(reservations)

def get_slot_to_cancel(number_of_slots):
    while True:
        try:
            print("Enter '0' to cancel the operation.")
            if number_of_slots == 1:
                slot = int(input(f"Slot number to cancel [1]: "))
            else:
                slot = int(input(f"Slot number to cancel [1 - {number_of_slots}]: "))
            if 0 <= slot <= number_of_slots:
                return slot
            else:
                print("#" * 62)
                print("##  Invalid slot number. Please enter a valid slot number.  ##")
                print("#" * 62, "\n")
        except ValueError:
            print("#" * 43)
            print("##  Invalid input. Enter integers only!  ##")
            print("#" * 43, "\n")

def confirm_cancellation():
    while True:
        try:
            confirm = input("Enter 'Y' to confirm cancellation or 'N' to cancel cancellation: ").strip().upper()
            if confirm == "Y":
                return True
            elif confirm == "N":
                clear_screen()
                print("#" * 29)
                print("##  Cancellation aborted.  ##")
                print("#" * 29, "\n")
                return False
            else:
                print("#" * 23)
                print("##  Invalid choice.  ##")
                print("#" * 23, "\n")
        except ValueError:
            print("#" * 47)
            print("##  Invalid input. Please enter 'Y' or 'N'.  ##")
            print("#" * 47, "\n")

def cancel_slot(input_date, slot, reservation_list):
    while True:
        if slot == 0:
            print("#" * 29)
            print("##  Cancellation aborted.  ##")
            print("#" * 29, "\n")
            break
        if not confirm_cancellation():
            break
        else:
            reservations = reservation_list[1:]
            for reservation in reservations:
                if input_date == reservation[0]:
                    index = reservations.index(reservation)
                    if len(reservation_list[index + 1]) == 2:
                        reservation_list.pop(index + 1)
                    else:
                        reservation_list[index + 1].pop(slot)
                    print("#" * 77)
                    print(f"##  Reservation for Slot {slot} on {input_date} has been cancelled successfully!  ##")
                    print("#" * 77, "\n")
            break
    
def get_date_input_to_cancel(reservation_list):
    while True:
        try:
            date_list = get_date_list(reservation_list)
            input_date = input("Enter the reservation date you wish to cancel (YYYY-MM-DD): ").strip()
            if input_date in date_list:
                return input_date
            else:
                print("#" * 95)
                print("##  There might be no reservation on this date or invalid input. Please enter a valid date.  ##")
                print("#" * 95, "\n")
        except ValueError:
            print("#" * 43)
            print("##  Invalid input. Enter integers only!  ##")
            print("#" * 43, "\n")

















############################################
############ UPDATE RESERVATION ############
############################################
def display_update_reservation(reservation_list):
    reservations = reservation_list[1:]

    print("-" * 83)
    print("|" + " " * 27 + f"Reservations for {reservation_list[0]}" + " " * 27 + "|")
    print("-" * 83)
    print(f"| Slot No. | Name               | Email                       | Phone No.   | Pax |")
    print("-" * 83)

    for i, reservation in enumerate(reservations, start=1):
        slot, name, email, phone, pax = reservation
        print(f"[{i:<1}] {slot:<6} | {name:<18} | {email:<27} | {phone:<11} | {pax:^3} |")
    print("-" * 83, "\n")

    return len(reservations)
def get_date_input_to_update(reservation_list):
    while True:
        try:
            date_list = get_date_list(reservation_list)
            input_date = input("Enter the reservation date you wish to update (YYYY-MM-DD): ").strip()
            if input_date in date_list:
                return input_date
            else:
                print("#" * 95)
                print("##  There might be no reservation on this date or invalid input. Please enter a valid date.  ##")
                print("#" * 95, "\n")
        except ValueError:
            print("#" * 43)
            print("##  Invalid input. Enter integers only!  ##")
            print("#" * 43, "\n")

def get_slot_to_update(number_of_slots):
    while True:
        try:
            print("Enter '0' to cancel the operation.")
            if number_of_slots == 1:
                slot = int(input(f"Slot number to update [1]: "))
            else:
                slot = int(input(f"Slot number to update [1 - {number_of_slots}]: "))
            if 0 <= slot <= number_of_slots:
                return slot
            else:
                print("#" * 62)
                print("##  Invalid slot number. Please enter a valid slot number.  ##")
                print("#" * 62, "\n")
        except ValueError:
            print("#" * 43)
            print("## Invalid input. Enter integers only!  ##")
            print("#" * 43, "\n")

def validate_date_input(date_string):
    if len(date_string) == 10:
        year = date_string[:4]
        month = date_string[5:7]
        day = date_string[8:10]
        if year.isdigit() and month.isdigit() and day.isdigit():
            if int(month) in range(1, 13) and int(day) in range(1, 32):
                return True
    return False

def display_reservation_information(reservation, date, is_update=True):
    #clear_screen()
    if is_update:
        print("Details for the reservation you wish to update:")
    else:
        print("Confirm your reservation details:")
    print(f"[1] Date{':':>11}", date)
    print(f"[2] Slot Number{':':>4}", reservation[0][5])
    print(f"[3] Name{':':>11}", reservation[1])
    print(f"[4] Email{':':>10}", reservation[2])
    print(f"[5] Phone Number{':':>3}", reservation[3])
    print(f"[6] Pax{':':>12}", reservation[4])
    print()

def update(slot, reservation, reservation_slot_list, reservation_list):
    date_list = get_date_list(reservation_list)
    
    new_date = reservation[0]
    reservation_information = reservation_list[date_list.index(new_date)][slot]
    reservation_slot = reservation_list[date_list.index(new_date)][1:]
    slot_number = slot
    while True:
        try:
            display_reservation_information(reservation_information, new_date)
            print("Enter the number of the detail you wish to update OR 'Y' to confirm changes OR 'N' to cancel: ")
            choice = input("Your choice: ").strip()
            if choice.upper() == "Y":
                print("#" * 78)
                print(f"##  Successfully update the reservation for {reservation_information[1]}, Slot {slot} on {reservation[0]}.  ##")
                print("#" * 78, "\n")
                break
            elif choice.upper() == "N":
                print("#" * 26)
                print("##  Changes cancelled.  ##")
                print("#" * 26, "\n")
                break
            elif 1 <= int(choice) <= 6:
                if int(choice) == 1:
                    clear_screen()
                    new_date = input("Enter new date (YYYY-MM-DD): ")
                    if validate_date_input(new_date):
                        if new_date == reservation[0]:
                            print("You have entered the same date as the original reservation.")
                        else:
                            if new_date not in date_list:
                                date_list.append(new_date)
                                reservation_list.append([new_date])
                            if len(reservation) == 2:
                                new_reservation_information_list = reservation[1]
                                if len(reservation_list[date_list.index(new_date)]) < 9:
                                    reservation_list.pop(date_list.index(reservation[0]))
                                    new_date_reservation_list = reservation_list[date_list.index(new_date) - 1]
                                    new_date_reservation_list.append(new_reservation_information_list)
                                    slot_number = len(new_date_reservation_list) - 1
                                else:
                                    print("#" * 40)
                                    print(f"##  No available slots on {new_date}  ##")
                                    print("#" * 40, "\n")
                                    new_date = reservation[0]
                            else:
                                new_reservation_information_list = reservation[slot]
                                if len(reservation_list[date_list.index(new_date)]) < 9:
                                    reservation.pop(slot)
                                    new_date_reservation_list = reservation_list[date_list.index(new_date)]
                                    new_date_reservation_list.append(new_reservation_information_list)
                                    slot_number = len(new_date_reservation_list) - 1
                                else:
                                    print("#" * 40)
                                    print(f"##  No available slots on {new_date}  ##")
                                    print("#" * 40, "\n")
                                    new_date = reservation[0]
                            #print(reservation_list)
                    elif not validate_date_input(new_date):
                        clear_screen()
                        print("#" * 48)
                        print("##  Invalid date. Please enter a valid date.  ##")
                        print("#" * 48)
                        new_date = reservation[0]
                elif int(choice) == 2:
                    clear_screen()
                    slot1, slot2, slot3, slot4 = check_available_slots(reservation_slot)
                    entry = get_slot(slot1, slot2, slot3, slot4)
                    reservation_information[0] = entry
                    clear_screen()
                elif int(choice) == 3:
                    clear_screen()
                    reservation_information[1] = get_name()
                    clear_screen()
                elif int(choice) == 4:
                    clear_screen()
                    reservation_information[2] = get_email()
                    clear_screen()
                elif int(choice) == 5:
                    clear_screen()
                    reservation_information[3] = get_phone_number()
                    clear_screen()
                elif int(choice) == 6:
                    clear_screen()
                    reservation_information[4] = get_pax()
                    clear_screen()
                print()
            else:
                clear_screen()
                print("#" * 53)
                print("##  Invalid choice. Please enter a valid choice.  ##")
                print("#" * 53, "\n")
        except ValueError:
            clear_screen()
            print("#" * 57)
            print("##  Invalid input. Enter integers or 'Y' or 'N' only!  ##")
            print("#" * 57, "\n")
