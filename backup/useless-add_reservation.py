import os

def clear_screen():
     os.system("cls" if os.name == "nt" else "clear")
     print()

def check_available_slots(reservation_list):
    available_slots = {
        "Slot 1": 2,
        "Slot 2": 2,
        "Slot 3": 2,
        "Slot 4": 2
    }
    for reservation in reservation_list:
        slot = reservation[0]
        if slot in available_slots:
            available_slots[slot] -= 1
    return available_slots

def get_slot_time(slot):
    slot_times = {
        "Slot 1": "12:00 pm - 02:00 pm",
        "Slot 2": "02:00 pm - 04:00 pm",
        "Slot 3": "06:00 pm - 08:00 pm",
        "Slot 4": "08:00 pm - 10:00 pm"
    }
    slot_number = str(next((index + 1 for index, value in enumerate(slot_times) if value == slot), None))
    time = slot_times.get(slot, 'Unknown Time')
    return time, slot_number

def get_slot(available_slots):
    print("These are the available slots for the date you entered: ")
    i = 1
    ret = []
    for slot, count in available_slots.items():
        if count > 0:
            time, target_slot = get_slot_time(slot)
            print(f"[{i}] {slot}: {time}")
            ret.append(target_slot)
            i += 1
    while True:
        try:
            if i == 2:
                slot = int(input(f"Enter the slot of the reservation[1]: "))
            else:
                slot = int(input(f"Enter the slot of the reservation[1 - {i - 1}]: "))
            if 1 <= slot < i:
                return f"Slot {ret[slot - 1]}"
            else:
                print("#" * 20)
                print("##  Out of slot!  ##")
                print("#" * 20, "\n")
        except ValueError:
            print("#" * 42)
            print("## Invalid input. Enter integers only!  ##")
            print("#" * 42, "\n")

def check_name(name):
    return name.replace(" ", "").isalpha()

def get_name():
    clear_screen()
    while True:
        name = input("Enter customer name: ").strip().upper()
        if check_name(name):
            return name
        else:
            print("#" * 59)
            print("##  Invalid name. Please enter letters and spaces only.  ##")
            print("#" * 59, "\n")

def check_email(email):
    if "@" in email and "." in email:
        at_index = email.index("@")
        dot_index = email.index(".")
        if dot_index - at_index > 1  and at_index > 0 and dot_index < len(email) - 1:
            return True
    return False

def get_email():
    clear_screen()
    while True:
        email = input("Enter customer email: ").strip()
        if check_email(email):
            return email
        else:
            print("#" * 50)
            print("##  Invalid email. Please enter a valid email.  ##")
            print("#" * 50, "\n")

def check_phone_number(phone):
    return (len(phone) == 10 and phone.startswith("01") and not phone.startswith("011") and not phone.startswith("015")) \
        or (len(phone) == 11 and (phone.startswith("011") or phone.startswith("015")))

def get_phone_number():
    clear_screen()
    while True:
        phone = input("Enter customer phone number: ").strip()
        if phone.isdigit() and check_phone_number(phone):
            return phone
        else:
            print("#" * 64)
            print("##  Invalid phone number. Please enter 10 or 11 digits only.  ##")
            print("#" * 64, "\n")

def get_pax():
    clear_screen()
    while True:
        try:
            pax = int(input("Enter number of pax: "))
            if 0 < pax <= 4:
                return str(pax)
            else:
                print("#" * 37)
                print("##  You may only enter 1 - 4 pax.  ##")
                print("#" * 37, "\n")
        except ValueError:
            print("#" * 43)
            print("##  Invalid input. Enter integers only!  ##")
            print("#" * 43, "\n")

def confirm_add_reservation():
    while True:
        choice = input("Confirm add reservation? [Y/N]: ").strip().upper()
        if choice == "Y":
            return True
        elif choice == "N":
            return False
        else:
            print("#" * 41)
            print("##  Invalid input. Enter Y or N only!  ##")
            print("#" * 41, "\n")
