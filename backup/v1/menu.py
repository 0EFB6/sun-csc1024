def get_menu_option():
    while True:
        try:
            option = input("Your choice: ")
            if 1 <= int(option) <= 6:
                return (option)
            else:
                print("#" * 34)
                print("# Enter integer between 1 and 6! #")
                print("#" * 34)
        except ValueError:
            print("Invalid input. Please enter integers only!")