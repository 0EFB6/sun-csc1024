import os

def clear_screen():
     os.system("cls" if os.name == "nt" else "clear")
     print()
     
def read_from_menu():
    with open("menuItems_23026149.txt", "r") as file:
        menuList = file.readlines()
        for i in range(len(menuList)):
            menuList[i] = menuList[i].strip("\n")
    return menuList

def read_from_reservation(file_name="reservation.txt"):
    reservationList = []
    with open(file_name, "r") as file:
        reservations = file.readlines()
        for reservation in reservations:
            reservationList.append(reservation.strip("\n").split("|"))
    reservationList.sort()
    return reservationList

def get_date_list(reservationList):
    # Create a list to store dates
    dateList = []
    for reservation in reservationList:
        date = reservation[0]
        dateList.append(date) if date not in dateList else None
    dateList.sort()

    # Return the date list
    return dateList

def organizeList(dateList, reservationList):
    # Create a list of lists to store reservations by date
    organizeList = []
    for date in dateList:
        dateReservations = [date]
        for reservation in reservationList:
            if reservation[0] == date:
                dateReservations.append(reservation[1:])
        organizeList.append(dateReservations)
    organizeList.sort()
    return organizeList

def press_enter_to_continue():
	input("Press Enter to continue...")

def prompt_switch():
    while True:
        print()
        try:            
            print("Enter 'Y' to continue perform current operation or 'N' to return to main menu:")
            switch = input("Your choice: ").strip().upper()
            if switch == "Y":
                return True
            elif switch == "N":
                return False
            else:
                print("Invalid choice.")
                continue
        except ValueError:
            print("Invalid choice. Please enter 'Y' or 'N'.")
            continue














############################################
############ CANCEL RESERVATION ############
############################################
def display_cancel_reservation(reservation_list):
    reservations = reservation_list[1:]

    print("-" * 83)
    print("|" + " " * 27 + f"Reservations for {reservation_list[0]}" + " " * 27 + "|")
    print("-" * 83)
    print(f"| Slot No. | Name               | Email                       | Phone No.   | Pax |")
    print("-" * 83)

    for i, reservation in enumerate(reservations, start=1):
        slot, name, email, phone, pax = reservation
        print(f"[{i:<1}] {slot:<6} | {name:<18} | {email:<27} | {phone:<11} | {pax:^3} |")
    print("-" * 83, "\n")

    return len(reservations)

def get_slot_to_cancel(number_of_slots):
    while True:
        try:
            print("Enter '0' to cancel the operation.")
            if number_of_slots == 1:
                slot = int(input(f"Slot number to cancel [1]: "))
            else:
                slot = int(input(f"Slot number to cancel [1 - {number_of_slots}]: "))
            if 0 <= slot <= number_of_slots:
                return slot
            else:
                print("Invalid slot number. Please enter a valid slot number.")               
        except ValueError:
            print("Invalid input. Enter integers only!")

def confirm_cancellation():
    while True:
        try:
            confirm = input("Enter 'Y' to confirm cancellation or 'N' to cancel cancellation: ").strip().upper()
            if confirm == "Y":
                return True
            elif confirm == "N":
                print("\nCancellation aborted.")
                return False
            else:
                print("Invalid choice.")
        except ValueError:
            print("Invalid input. Please enter 'Y' or 'N'.")

def cancel_slot(input_date, slot, reservation_list):
    while True:
        if slot == 0:
            print("\nCancellation aborted.")
            break
        if not confirm_cancellation():
            break
        else:
            reservations = reservation_list[1:]
            for reservation in reservations:
                if input_date == reservation[0]:
                    index = reservations.index(reservation)
                    if len(reservation_list[index + 1]) == 2:
                        reservation_list.pop(index + 1)
                    else:
                        reservation_list[index + 1].pop(slot)
                    print(f"Reservation for Slot {slot} on {input_date} has been cancelled successfully!")
            break
    
def get_date_input(reservation_list):
    while True:
        try:
            date_list = get_date_list(reservation_list)
            input_date = input("Enter the reservation date you wish to cancel (YYYY-MM-DD): ").strip()
            if input_date in date_list:
                return input_date
            else:
                print("There might be no reservation on this date or invalid input. Please enter a valid date.")
        except ValueError:
            print("Invalid input. Enter integers only!")











############################################
############ UPDATE RESERVATION ############
############################################