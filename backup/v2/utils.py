import os
from add_reservation import check_available_slots, get_slot

def clear_screen():
     os.system("cls" if os.name == "nt" else "clear")
     print()
     
def read_from_menu():
    with open("menuItems_23026149.txt", "r") as file:
        menuList = file.readlines()
        for i in range(len(menuList)):
            menuList[i] = menuList[i].strip("\n")
    return menuList

def read_from_reservation(file_name="reservation.txt"):
    reservationList = []
    with open(file_name, "r") as file:
        reservations = file.readlines()
        for reservation in reservations:
            reservationList.append(reservation.strip("\n").split("|"))
    reservationList.sort()
    return reservationList

def get_date_list(reservationList):
    # Create a list to store dates
    dateList = []
    for reservation in reservationList:
        date = reservation[0]
        dateList.append(date) if date not in dateList else None
    dateList.sort()

    # Return the date list
    return dateList

def organizeList(dateList, reservationList):
    # Create a list of lists to store reservations by date
    organizeList = []
    for date in dateList:
        dateReservations = [date]
        for reservation in reservationList:
            if reservation[0] == date:
                dateReservations.append(reservation[1:])
        organizeList.append(dateReservations)
    organizeList.sort()
    return organizeList

def press_enter_to_continue():
	input("Press Enter to continue...")

def prompt_switch():
    while True:
        print()
        try:            
            print("Enter 'Y' to continue perform current operation or 'N' to return to main menu")
            switch = input("Your choice: ").strip().upper()
            if switch == "Y":
                return True
            elif switch == "N":
                return False
            print("Invalid choice.")
        except ValueError:
            print("Invalid choice. Please enter 'Y' or 'N'.")














############################################
############ CANCEL RESERVATION ############
############################################
def display_cancel_reservation(reservation_list):
    reservations = reservation_list[1:]

    print("-" * 83)
    print("|" + " " * 27 + f"Reservations for {reservation_list[0]}" + " " * 27 + "|")
    print("-" * 83)
    print(f"| Slot No. | Name               | Email                       | Phone No.   | Pax |")
    print("-" * 83)

    for i, reservation in enumerate(reservations, start=1):
        slot, name, email, phone, pax = reservation
        print(f"[{i:<1}] {slot:<6} | {name:<18} | {email:<27} | {phone:<11} | {pax:^3} |")
    print("-" * 83, "\n")

    return len(reservations)

def get_slot_to_cancel(number_of_slots):
    while True:
        try:
            print("Enter '0' to cancel the operation.")
            if number_of_slots == 1:
                slot = int(input(f"Slot number to cancel [1]: "))
            else:
                slot = int(input(f"Slot number to cancel [1 - {number_of_slots}]: "))
            if 0 <= slot <= number_of_slots:
                return slot
            else:
                print("Invalid slot number. Please enter a valid slot number.")               
        except ValueError:
            print("Invalid input. Enter integers only!")

def confirm_cancellation():
    while True:
        try:
            confirm = input("Enter 'Y' to confirm cancellation or 'N' to cancel cancellation: ").strip().upper()
            if confirm == "Y":
                return True
            elif confirm == "N":
                print("\nCancellation aborted.")
                return False
            else:
                print("Invalid choice.")
        except ValueError:
            print("Invalid input. Please enter 'Y' or 'N'.")

def cancel_slot(input_date, slot, reservation_list):
    while True:
        if slot == 0:
            print("\nCancellation aborted.")
            break
        if not confirm_cancellation():
            break
        else:
            reservations = reservation_list[1:]
            for reservation in reservations:
                if input_date == reservation[0]:
                    index = reservations.index(reservation)
                    if len(reservation_list[index + 1]) == 2:
                        reservation_list.pop(index + 1)
                    else:
                        reservation_list[index + 1].pop(slot)
                    print(f"Reservation for Slot {slot} on {input_date} has been cancelled successfully!")
            break
    
def get_date_input_to_cancel(reservation_list):
    while True:
        try:
            date_list = get_date_list(reservation_list)
            input_date = input("Enter the reservation date you wish to cancel (YYYY-MM-DD): ").strip()
            if input_date in date_list:
                return input_date
            else:
                print("There might be no reservation on this date or invalid input. Please enter a valid date.")
        except ValueError:
            print("Invalid input. Enter integers only!")

















############################################
############ UPDATE RESERVATION ############
############################################
def display_update_reservation(reservation_list):
    reservations = reservation_list[1:]

    print("-" * 83)
    print("|" + " " * 27 + f"Reservations for {reservation_list[0]}" + " " * 27 + "|")
    print("-" * 83)
    print(f"| Slot No. | Name               | Email                       | Phone No.   | Pax |")
    print("-" * 83)

    for i, reservation in enumerate(reservations, start=1):
        slot, name, email, phone, pax = reservation
        print(f"[{i:<1}] {slot:<6} | {name:<18} | {email:<27} | {phone:<11} | {pax:^3} |")
    print("-" * 83, "\n")

    return len(reservations)
def get_date_input_to_update(reservation_list):
    while True:
        try:
            date_list = get_date_list(reservation_list)
            input_date = input("Enter the reservation date you wish to update (YYYY-MM-DD): ").strip()
            if input_date in date_list:
                return input_date
            else:
                print("There might be no reservation on this date or invalid input. Please enter a valid date.")
        except ValueError:
            print("Invalid input. Enter integers only!")

def get_slot_to_update(number_of_slots):
    while True:
        try:
            print("Enter '0' to cancel the operation.")
            if number_of_slots == 1:
                slot = int(input(f"Slot number to update [1]: "))
            else:
                slot = int(input(f"Slot number to update [1 - {number_of_slots}]: "))
            if 0 <= slot <= number_of_slots:
                return slot
            else:
                print("Invalid slot number. Please enter a valid slot number.")               
        except ValueError:
            print("Invalid input. Enter integers only!")

def validate_date_input(date_string):
    if len(date_string) == 10:
        year = date_string[:4]
        month = date_string[5:7]
        day = date_string[8:10]
        if year.isdigit() and month.isdigit() and day.isdigit():
            if int(month) in range(1, 13) and int(day) in range(1, 32):
                return True
    return False

def display_reservation_information(slot, reservation, date):
    #clear_screen()
    print("Details for the reservation you wish to update:")
    print(f"[1] Date{':':>11}", date)
    print(f"[2] Slot Number{':':>4}", reservation[0][5])
    print(f"[3] Name{':':>11}", reservation[1])
    print(f"[4] Email{':':>10}", reservation[2])
    print(f"[5] Phone Number{':':>3}", reservation[3])
    print(f"[6] Pax{':':>12}", reservation[4])
    print()

def update(slot, reservation, reservation_slot_list, reservation_list):
    date_list = get_date_list(reservation_list)
    
    new_date = reservation[0]
    reservation_information = reservation_list[date_list.index(new_date)][slot]
    reservation_slot = reservation_list[date_list.index(new_date)][1:]
    slot_number = slot
    while True:
        try:
            display_reservation_information(slot_number, reservation_information, new_date)
            print("Enter the number of the detail you wish to update OR 'Y' to confirm changes OR 'N' to cancel: ")
            choice = input("Your choice: ").strip()
            if choice.upper() == "Y":
                print(f"\nSuccessfully update the reservation for {reservation_information[1]}, Slot {slot} on {reservation[0]}.")
                break
            elif choice.upper() == "N":
                print("\nChanges cancelled.")
                break
            elif 1 <= int(choice) <= 6:
                if int(choice) == 1:
                    new_date = input("Enter new date (YYYY-MM-DD): ")
                    if validate_date_input(new_date):
                        if new_date == reservation[0]:
                            print("You have entered the same date as the original reservation.")
                        else:
                            if new_date not in date_list:
                                date_list.append(new_date)
                                reservation_list.append([new_date])
                            if len(reservation) == 2:
                                new_reservation_information_list = reservation[1]
                                reservation_list.pop(date_list.index(reservation[0]))
                                new_date_reservation_list = reservation_list[date_list.index(new_date) - 1]
                                new_date_reservation_list.append(new_reservation_information_list)
                                slot_number = len(new_date_reservation_list) - 1
                            else:
                                new_reservation_information_list = reservation[slot]
                                reservation.pop(slot)
                                new_date_reservation_list = reservation_list[date_list.index(new_date)]
                                new_date_reservation_list.append(new_reservation_information_list)
                                slot_number = len(new_date_reservation_list) - 1
                            print(reservation_list)
                    elif not validate_date_input(new_date):
                        print("Invalid date. Please enter a valid date.")
                        new_date = reservation[0]
                elif int(choice) == 2:
                    slot1, slot2, slot3, slot4 = check_available_slots(reservation_slot)
                    entry = get_slot(slot1, slot2, slot3, slot4)
                    reservation_information[0] = entry
                elif int(choice) == 3:
                    name = input("Enter new name: ")
                    reservation[1][int(choice) - 1] = name
                elif int(choice) == 4:
                    email = input("Enter new email: ")
                    reservation[1][int(choice) - 1] = email
                elif int(choice) == 5:
                    phone = input("Enter phone number: ")
                    reservation[1][int(choice) - 1] = phone
                elif int(choice) == 6:
                    pax = int(input("Enter new pax: "))
                    reservation[1][int(choice) - 1] = pax
                print()
                print(reservation)
            else:
                clear_screen()
                print("Invalid choice. Please enter a valid choice.\n")
        except ValueError:
            print("Invalid input. Enter integers or 'Y' or 'N' only!")
