# CSC 1024 Python Final Assignment
# By Group: Code Overflow
# Members: 23026149,
# Date: 2023-07-24
# Description: This program is a restaurant reservation system that allows
#              users to make, update and cancel reservations. It also
#              generates random recommendations of dishes from the menu.
# Files: main.py, utils.py, menuItems_23026149.txt, reservation_23026149.txt
#        (menuItems_23026149.txt and reservation_23026149.txt are sample files
#        for testing purposes only.)
# Current File: main.py
# Functions: menu(), add_reservation(), cancel_reservation(),
#            update_reservation(), display(), generate_recommendation(),
#            main()
# Number of functions: 7

import random
from utils import *
from datetime import datetime

# MAIN MENU FUNCTION: Handles the main menu and user interactions.


def menu():
    # Read reservation details from file and organize them based on date.
    res_file = "reservation_23026149.txt"
    date_list = generate_date_list(read_from_reservation_file(res_file))
    res_file_list = read_from_reservation_file(res_file)
    reservation_list = generate_organized_list(date_list, res_file_list)

    # Display the main menu options and process the user's choice by using if,
    # elif and else statement. Use While Loop to keep the program running
    # until the user chooses to exit.
    while True:
        display_menu()
        option = get_menu_selection()
        if option == 1:
            add_reservation(reservation_list)
        elif option == 2:
            cancel_reservation(reservation_list)
        elif option == 3:
            update_reservation(reservation_list)
        elif option == 4:
            display(reservation_list)
            press_enter_to_continue()
        elif option == 5:
            generate_recommendation()
            press_enter_to_continue()
        elif option == 6:
            write_reservation_data_to_file(reservation_list, res_file)
            clear_screen()
            exit("Goodbye!\n")

# ADD RESERVATION FUNCTION: Handles the process of adding
#                           a new reservation.


def add_reservation(reservation_list):
    # Initialize a trigger to clear the screen when needed.
    trigger_clear_screen = True

    # Use While Loop to collect reservation information from the user.
    while True:
        reservation = []

        # Get the list of existing reservation dates to easily
        # locate the index in reservation_list.
        date_list = generate_date_list(reservation_list)

        # Get the list of existing names to prevent multiple
        # reservations made by same person.
        name_list = generate_name_list(reservation_list)

        # Clear the screen when needed.
        if trigger_clear_screen:
            clear_screen()

        # Get the date input from the user, strip unexpected spaces.
        date_input_message = "Enter the reservation date (YYYY-MM-DD): "
        date_input = input(date_input_message).strip()

        # Validate the user's date input, reprompt if it's invalid.
        if not validate_date(mode="add", user_input=date_input):
            trigger_clear_screen = False
            continue

        # Check if the date is new or already exists in the
        # date list. Add the date to the date list if it's new.
        is_new_date = False
        if date_input not in date_list:
            date_list.append(date_input)
            # If the date is new, get the slot input from the user and
            # show all available slots to user. Argument passed to
            # check_available_slot() is None because there is no
            # existing reservation on the new date.
            avail_slot = check_available_slot(None, None, None, None, True)
            slot = get_slot(mode="add", available_slots=avail_slot)
            is_new_date = True
        else:
            # Locate the reservation in reservation_list based on
            # the date input.
            reservation = reservation_list[date_list.index(date_input)][1:]
            if len(reservation) >= 32:
                # If there are no available slots for the existing
                # date, reprompt the user for a new date.
                print("#" * 80)
                print(f"##  There are no available slots for {date_input}. \
Please choose another date.  ##")
                print("#" * 80, "\n")
                trigger_clear_screen = False
                continue
            # Get slot input from user and check the available slots.
            avail_slot = check_available_slot(None, reservation)
            slot = get_slot(mode="add", available_slots=avail_slot)

        # Get other necessary information from the user and store it
        # in a temporary list. Then display the reservation information
        # to the user for confirmation.
        info_list = [
            slot,
            get_name(name_list),
            get_email(),
            get_phone_number(),
            get_pax()
        ]
        clear_screen()
        display_reservation_information(info_list, date_input, False)

        # Sort the date list to make sure reservation list is organized.
        date_list.sort()

        # Prompt the user for confirmation, if yes, add the information
        # list to the reservation list and display success message.
        # If no, ignore reservations details entered earlier and prompt
        # the user for multiple operations or exit.
        if confirm_prompt(mode="add"):
            pos = date_list.index(date_input)
            if is_new_date:
                reservation_list.insert(pos, [date_input, info_list])
            else:
                reservation_list[date_list.index(date_input)].append(info_list)
            clear_screen()
            print("#" * 74)
            print(f"##   Reservation for {slot} on {date_input} has \
been made successfully!   ##")
            print("#" * 74)

        # Prompt user for multiple operations or exit
        if not get_prompt_switch():
            break
        else:
            trigger_clear_screen = True

# CANCEL RESERVATION FUNCTION: Allows users to cancel a
#                              reservation for a specific date and slot.


def cancel_reservation(reservation_list):
    while True:
        # Clear screen & get date input from user and validate input
        clear_screen()
        input_date = get_date_input("cancel", reservation_list)
        clear_screen()

        # Check if any reservation matches the input date
        for reservation in reservation_list:
            if input_date == reservation[0]:

                # Display reservation information for the selected date
                number_of_slots = display_reservations(reservation)

                # Get slot input to cancel from user
                slot = get_slot("cancel_operation", None, None, 0,
                                number_of_slots)
                clear_screen()

                # Remove reservation from the reservation list for the
                # selected date and slot
                cancel_slot(reservation_list, input_date, slot)

        # Prompt user for multiple operations or exit the looop
        if not get_prompt_switch():
            break

# UPDATE RESERVATION FUNCTION: Function to update a reservation in the
#                              reservation_list


def update_reservation(reservation_list):
    while True:
        # Clear the screen & get the date input from the user
        # and validate input
        clear_screen()
        input_date = get_date_input(mode="update",
                                    reservation_list=reservation_list)
        clear_screen()

        # Check if any reservation matches the input date
        for reservation in reservation_list:
            if input_date == reservation[0]:
                # Get a list with all reservations on the input date
                # excluding the date
                reservation_slot_list = reservation[1:]

                # Display reservation information for the selected date
                display_reservations(reservation)

                # Get the slot input to update from the user, cancel
                # operation if 0
                slot = get_slot(mode="update_operation", available_slots=None,
                                date=None, count=0,
                                no_of_slot=len(reservation_slot_list))
                if slot == 0:
                    break

                # Cleart the screen and generate a list with dates only
                # which contains reservation for functions call usage
                clear_screen()
                date_list = generate_date_list(reservation_list)

                # Original reservation information according to slot
                # entered by the user before changes
                # For printing the success message purposes
                index = date_list.index(input_date)
                original_information = reservation_list[index][slot]

                # Reservation information that is subject to change
                # after the user chooses to update certain information
                # Act as a temporary list to store changes before
                # actually writing to the main reservatin list
                index = date_list.index(input_date)
                amended_changes = reservation_list[index][slot][:]

                # Original reservations information on the original
                # date excluding the date
                index = date_list.index(reservation[0])
                reservation_slots_orig = reservation_list[index][1:]

                # Reservation information that is subject to change
                # after the user chooses to update certain information
                # inside the while loop
                reservation_slots = [item[:]
                                     for item in reservation_slots_orig]

                # Variable to store the original date, subject to
                # change if the user chooses to change the date
                new_date = reservation[0]

                while True:
                    # Display updated reservation information to the
                    # user for confirmation
                    display_reservation_information(amended_changes,
                                                    new_date, is_update=True)

                    # Get user input for information selection to
                    # update (N = No, Y = Yes, 1 = Date
                    # 2 = Name, 3 = Email, 4 = Phone Number, 5 = Pax, 6 = Date)
                    selection = get_selection_to_update()

                    if selection in ["N", "Y"]:
                        if selection == "Y":
                            # Write changes to the main reservation list
                            date_index = date_list.index(input_date)
                            write_changes(reservation_list, amended_changes,
                                          date_index, slot, new_date)

                            # Display the success message
                            sp = (18 - len(original_information[1])) // 2 * ' '
                            name = original_information[1]
                            slot = " " + original_information[0] + " "
                            date = " " + new_date
                            msg = "  Successfully update the reservation for "
                            print("#" * 89)
                            print(f"##{msg}{sp}{name}{sp},{slot}on{date}.  ##")
                            print("#" * 89, "\n")
                        break

                    # Update the relevant selection according to the
                    # user input
                    new_date = update(selection, reservation_list,
                                      amended_changes, slot, reservation,
                                      reservation_slots, new_date)

        # Prompt the user for multiple operations or exit
        if not get_prompt_switch():
            break

# DISPLAY RESERVATION FUNCTION: Function to display all reservation
#                               information


def display(reservation_list):
    clear_screen()

    # Sort the reservation list by slot number and then display
    # reservation information for each date by using for loop.
    sorted_reservation_list = [
        sorted(item, key=lambda x: x[0]) for item in reservation_list
    ]

    for date, *reservations in sorted_reservation_list:
        print("-" * 81)
        print(f"|{' ' * 34}{date}{' ' * 35}|")
        print("-" * 81)
        for slot, name, email, hp, pax in reservations:
            print(f"| {slot:<6} | {name:<19} | {email:<28} \
| {hp:<11} | {pax} |")
        print("-" * 81)
        print()

# GENERATE RECOMMENDATION FUNCTION: Function to generate
#                                   random recommendation of dishes


def generate_recommendation():
    # Get the menu list from the menu file and display a random
    # set of recommended dishes
    menu_list = read_from_menu_file("menuItems_23026149.txt")
    clear_screen()

    while True:
        try:
            count = int(input("How many recommended dishes do you want? "))

            # Check if the input count is within the valid range of
            # available dishes
            if count < 1 or count > len(menu_list):
                print("#" * 64)
                print(f"##  Please enter a number greater than 0 and \
lesser than {len(menu_list) + 1:<2}.  ##")
                print("#" * 64, "\n")
            else:
                break
        except ValueError:
            # Handle the case when the user enters a non-integer value
            print("#" * 53)
            print("##  Please enter a valid number (integers ONLY!).  ##")
            print("#" * 53, "\n")

    clear_screen()
    # Randomly select dishes from the menu list based on the number
    # requested by the user
    recommended_dishes = random.sample(menu_list, count)

    # Print the randomly recommended dishes in a
    # decorative box using for loop
    print(" " * 20 + "#" * 24)
    print(" " * 20 + "##  Recommended dish  ##")
    print(" " * 20 + "#" * 24, "\n")
    print(f"     {'-' * 61}      ")
    print(f"    / {' ' * 59} \\     ")
    for dish in recommended_dishes:
        print(f"   |       {dish:<50}      |    ")
    print(f"    \\ {' ' * 59} /     ")
    print(f"     {'-' * 61}      ")
    print()

# Define the main function to start the program.


def main():
    # Call the menu function to display the program's menu options.
    menu()

# The following block of code ensures that the main function is
# executed only when this script is run directly,
# not when it's imported as a module into another script.


if __name__ == "__main__":
    # Start the main function and execute the program.
    main()
