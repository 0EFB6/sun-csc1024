# CSC 1024 Python Final Assignment

import random
from utils import *
from datetime import datetime

def menu():
    reservation_file = "reservation_23026149.txt"
    reservation_list = generate_organized_list(generate_date_list(read_from_reservation_file(reservation_file)), read_from_reservation_file(reservation_file))
    while True:
        display_menu()
        option = get_menu_option()
        if option == 1:
            add_reservation(reservation_list)
        elif option == 2:
            cancel_reservation(reservation_list)
        elif option == 3:
            update_reservation(reservation_list)
        elif option == 4:
            display(reservation_list)
            press_enter_to_continue()
        elif option == 5:
            generate_recommendation()
            press_enter_to_continue()
        elif option == 6:
            write_reservation_data_to_file(reservation_list, reservation_file)
            clear_screen()
            exit("Goodbye!")

def add_reservation(reservation_list):
    trigger_clear_screen = True
    while True:
        reservation = []
        date_list = generate_date_list(reservation_list)
        if trigger_clear_screen:
            clear_screen()
        date_input = input("Enter the reservation date (YYYY-MM-DD): ").strip()
        if not validate_date(date_input, mode="add"):
            trigger_clear_screen = False
            continue
        is_new_date = False
        if date_input not in date_list:
            date_list.append(date_input)
            slot = get_slot(check_available_slots(None, True), mode="add")
            is_new_date = True
        else:
            reservation = reservation_list[date_list.index(date_input)][1:]
            if len(reservation) >= 8:
                print("#" * 80)
                print(f"##  There are no available slots for {date_input}. Please choose another date.  ##")
                print("#" * 80, "\n")
                trigger_clear_screen = False
                continue
            slot = get_slot(check_available_slots(reservation), mode="add")
        information_list = [
            slot,
            get_name(),
            get_email(),
            get_phone_number(),
            get_pax()
        ]
        clear_screen()
        display_reservation_information(information_list, date_input, is_update=False)
        if confirm_prompt(mode="add"):
            reservation_list.append([date_input, information_list]) if is_new_date else reservation.append(information_list)
            clear_screen()
            print("#" * 74)
            print(f"##   Reservation for {slot} on {date_input} has been made successfully!   ##")
            print("#" * 74)
        if not get_prompt_switch():
            break
        else:
            trigger_clear_screen = True

def cancel_reservation(reservation_list):
    while True:
        clear_screen()
        input_date = get_date_input(reservation_list, mode="cancel")
        clear_screen()
        for reservation in reservation_list:
            if input_date == reservation[0]:
                number_of_slots = display_reservations(reservation)
                slot = get_slot(available_slots=None, mode="cancel_operation", date=None, count=0, number_of_slots=number_of_slots)
                clear_screen()
                cancel_slot(input_date, slot, reservation_list)
        if not get_prompt_switch():
            break

def update_reservation(reservation_list):
    while True:
        clear_screen()
        input_date = get_date_input(reservation_list, mode="update")
        clear_screen()
        for reservation in reservation_list:
            if input_date == reservation[0]:
                reservation_slot_list = reservation[1:]
                display_reservations(reservation)
                slot = get_slot(available_slots=None, mode="update_operation", date=None, count=0, number_of_slots=len(reservation_slot_list))
                if slot == 0:
                    break
                clear_screen()
                date_list = generate_date_list(reservation_list)
                original_information = reservation_list[date_list.index(input_date)][slot]
                amended_changes = reservation_list[date_list.index(input_date)][slot][:]
                reservation_slots_orig = reservation_list[date_list.index(reservation[0])][1:]
                reservation_slots = [item[:] for item in reservation_slots_orig]
                new_date = reservation[0]
                while True:
                    display_reservation_information(amended_changes, new_date, is_update=True)
                    selection = get_selection_to_update()
                    if selection in ["N", "Y"]:
                        if selection == "Y":
                            date_index = date_list.index(input_date)
                            write_changes(slot, date_index, amended_changes, reservation_list, new_date)
                            padding = (18 - len(original_information[1])) // 2
                            print("#" * 89)
                            print(f"##  Successfully update the reservation for {' ' * padding}{original_information[1]}{' ' * padding}, {original_information[0]} on {input_date}.  ##")
                            print("#" * 89, "\n")
                        break
                    new_date = update(slot, reservation, reservation_list, selection, date_list, amended_changes, reservation_slots, new_date)        
        if not get_prompt_switch():
            break

def display(reservation_list):
    clear_screen()
    for date, *reservations in reservation_list:
        print("-" * 81)
        print(f"|{' ' * 34}{date}{' ' * 35}|")
        print("-" * 81)
        for slot, name, email, phone_number, pax in reservations:
            print(f"| {slot:<6} | {name:<19} | {email:<28} | {phone_number:<11} | {pax} |")
        print("-" * 81)
        print()
    
def generate_recommendation():
    menu_list = read_from_menu_file("menuItems_23026149.txt")
    clear_screen()
    print(" " * 20 + "#" * 26)
    print(" " * 20 + "##  Recommended dishes  ##")
    print(" " * 20 + "#" * 26, "\n")
    print("     -------------------------------------------------------------      ")
    print("    /                                                             \     ")
    print(f"   |       {random.choice(menu_list):<50}      |    ")
    print("    \                                                             /     ")
    print("     -------------------------------------------------------------      ")
    print()

def main():
    menu()

if __name__ == "__main__":
    main()
