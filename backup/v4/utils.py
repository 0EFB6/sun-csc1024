import os
from datetime import datetime, timedelta

# 33 Functions

############################################
################# Utilities ################
############################################
def clear_screen():
     os.system("cls" if os.name == "nt" else "clear")
     print()
    
def display_menu():
    clear_screen()
    print("Charming Thyme Trattoria Restaurant Management System")
    print("\n[1] Add Reservation(s)")
    print("[2] Cancel Reservation(s)")
    print("[3] Update/Edit Reservation(s)")
    print("[4] Display")
    print("[5] Generate Meal Recommendation")
    print("[6] Exit")
    print("-" * 36)
    print("| Enter a number (1-6) to continue |")
    print("-" * 36)

def get_date_list(reservation_list):
    # Return a list that stores dates from reservation_list
    return sorted(set(reservation[0] for reservation in reservation_list))

def get_organized_list(date_list, reservation_list):
    organized_list = []
    for date in date_list:
        organized_list.append([date] + [reservation[1:] for reservation in reservation_list if reservation[0] == date])
    return organized_list

def get_menu_option():
    while True:
        try:
            option = int(input("Your choice: "))
            if 1 <= option <= 6:
                return option
            else:
                print("#" * 34)
                print("# Enter integer between 1 and 6! #")
                print("#" * 34)
        except ValueError:
            print("#" * 50)
            print("##  Invalid input. Please enter integers only!  ##")
            print("#" * 50)

def press_enter_to_continue():
	input("Press Enter to continue...")

def prompt_switch():
    while True:
        print()
        switch = input("Enter 'Y' to continue perform current operation or 'N' to return to main menu: ").strip().upper()
        if switch == "Y":
            return True
        elif switch == "N":
            return False
        print("#" * 23)
        print("##  Invalid choice.  ##")
        print("#" * 23, "\n")

def read_from_menu_file():
    with open("menuItems_23026149.txt", "r") as file:
        return [menu.strip("\n") for menu in file.readlines()]

def read_from_reservation_file(file_name="reservation.txt"):
    with open(file_name, "r") as file:
        reservation_list = [reservation.strip("\n").split("|") for reservation in file.readlines()]
    reservation_list.sort()
    return reservation_list

def validate_reservation_date(user_input):
    try:
        input_date = datetime.strptime(user_input, "%Y-%m-%d").date()
        current_date = datetime.now().date()
        target_date = current_date + timedelta(days=8)

        if input_date >= target_date:
            return True
        else:
            print("#" * 78)
            print(f"##  The date you entered is invalid. Please enter a date after {(target_date - timedelta(days=1)).strftime('%Y-%m-%d')}.  ##")
            print("#" * 78, "\n")
            return False
    except ValueError:
        print("#" * 74)
        print("##  Invalid date format. Please enter a date in the YYYY-MM-DD format.  ##")
        print("#" * 74, "\n")
        return False
    
def write_reservation_data_to_file(reservation_list, file_name="reservation.txt"):
    formatted_reservations = [f'{date}|{slot}|{name}|{email}|{phone_number}|{pax}' 
                              for date, *slots in reservation_list 
                              for slot, name, email, phone_number, pax in slots]
    with open(file_name, "w") as file:
        file.write('\n'.join(formatted_reservations))













############################################
############## ADD RESERVATION #############
############################################
def check_available_slots(reservation_list):
    available_slots = {
        "Slot 1": 2,
        "Slot 2": 2,
        "Slot 3": 2,
        "Slot 4": 2
    }
    for reservation in reservation_list:
        slot = reservation[0]
        if slot in available_slots:
            available_slots[slot] -= 1
    return available_slots

def check_email(email):
    if "@" in email and "." in email:
        at_index = email.index("@")
        dot_index = email.index(".")
        if dot_index - at_index > 1  and at_index > 0 and dot_index < len(email) - 1:
            return True
    return False

def check_name(name):
    return name.replace(" ", "").isalpha()

def check_phone_number(phone):
    return (len(phone) == 10 and phone.startswith("01") and not phone.startswith("011") and not phone.startswith("015")) \
        or (len(phone) == 11 and (phone.startswith("011") or phone.startswith("015")))

def confirm_add_reservation():
    while True:
        choice = input("Confirm add reservation? [Y/N]: ").strip().upper()
        if choice == "Y":
            return True
        elif choice == "N":
            return False
        else:
            print("#" * 41)
            print("##  Invalid input. Enter Y or N only!  ##")
            print("#" * 41, "\n")

def get_email():
    clear_screen()
    while True:
        email = input("Enter customer email: ").strip()
        if check_email(email):
            return email
        else:
            print("#" * 50)
            print("##  Invalid email. Please enter a valid email.  ##")
            print("#" * 50, "\n")

def get_name():
    clear_screen()
    while True:
        name = input("Enter customer name: ").strip().upper()
        if check_name(name):
            return name
        else:
            print("#" * 59)
            print("##  Invalid name. Please enter letters and spaces only.  ##")
            print("#" * 59, "\n")

def get_pax():
    clear_screen()
    while True:
        try:
            pax = int(input("Enter number of pax: "))
            if 0 < pax <= 4:
                return str(pax)
            else:
                print("#" * 37)
                print("##  You may only enter 1 - 4 pax.  ##")
                print("#" * 37, "\n")
        except ValueError:
            print("#" * 43)
            print("##  Invalid input. Enter integers only!  ##")
            print("#" * 43, "\n")

def get_phone_number():
    clear_screen()
    while True:
        phone = input("Enter customer phone number: ").strip()
        if phone.isdigit() and check_phone_number(phone):
            return phone
        else:
            print("#" * 64)
            print("##  Invalid phone number. Please enter 10 or 11 digits only.  ##")
            print("#" * 64, "\n")

def get_slot_time(slot):
    slot_times = {
        "Slot 1": "12:00 pm - 02:00 pm",
        "Slot 2": "02:00 pm - 04:00 pm",
        "Slot 3": "06:00 pm - 08:00 pm",
        "Slot 4": "08:00 pm - 10:00 pm"
    }
    slot_number = str(next((index + 1 for index, value in enumerate(slot_times) if value == slot), None))
    time = slot_times.get(slot, 'Unknown Time')
    return time, slot_number

def get_slot(available_slots):
    print("These are the available slots for the date you entered: ")
    i = 1
    ret = []
    for slot, count in available_slots.items():
        if count > 0:
            time, target_slot = get_slot_time(slot)
            print(f"[{i}] {slot}: {time}")
            ret.append(target_slot)
            i += 1
    while True:
        try:
            if i == 2:
                slot = int(input(f"Enter the slot of the reservation[1]: "))
            else:
                slot = int(input(f"Enter the slot of the reservation[1 - {i - 1}]: "))
            if 1 <= slot < i:
                return f"Slot {ret[slot - 1]}"
            else:
                print("#" * 20)
                print("##  Out of slot!  ##")
                print("#" * 20, "\n")
        except ValueError:
            print("#" * 42)
            print("## Invalid input. Enter integers only!  ##")
            print("#" * 42, "\n")



































############################################
############ CANCEL RESERVATION ############
############################################
def confirm_cancellation():
    while True:
        try:
            confirm = input("Enter 'Y' to confirm cancellation or 'N' to cancel cancellation: ").strip().upper()
            if confirm == "Y":
                return True
            elif confirm == "N":
                clear_screen()
                print("#" * 29)
                print("##  Cancellation aborted.  ##")
                print("#" * 29, "\n")
                return False
            print("#" * 23)
            print("##  Invalid choice.  ##")
            print("#" * 23, "\n")
        except ValueError:
            print("#" * 47)
            print("##  Invalid input. Please enter 'Y' or 'N'.  ##")
            print("#" * 47, "\n")

def cancel_slot(input_date, slot, reservation_list):
    if slot == 0:
        print("#" * 29)
        print("##  Cancellation aborted.  ##")
        print("#" * 29, "\n")
        return
    if not confirm_cancellation():
        return

    reservations = reservation_list[1:]
    for reservation in reservations:
        if input_date == reservation[0]:
            index = reservations.index(reservation)
            if len(reservation_list[index + 1]) == 2:
                reservation_list.pop(index + 1)
            else:
                reservation_list[index + 1].pop(slot)
            print("#" * 77)
            print(f"##  Reservation for Slot {slot} on {input_date} has been cancelled successfully!  ##")
            print("#" * 77, "\n")

def display_cancel_reservation(reservation_list):
    print("-" * 83)
    print(f"|{'Reservations for ' + reservation_list[0]:^81}|")
    print("-" * 83)
    print("| Slot No. | Name               | Email                       | Phone No.   | Pax |")
    print("-" * 83)

    for i, reservation in enumerate(reservation_list[1:], start=1):
        slot, name, email, phone, pax = reservation
        print(f"[{i:<1}] {slot:<6} | {name:<18} | {email:<27} | {phone:<11} | {pax:^3} |")
    print("-" * 83, "\n")

    return len(reservation_list) - 1

def get_date_input_to_cancel(reservation_list):
    while True:
        try:
            date_list = get_date_list(reservation_list)
            input_date = input("Enter the reservation date you wish to cancel (YYYY-MM-DD): ").strip()
            if input_date in date_list:
                return input_date
            else:
                print("#" * 95)
                print("##  There might be no reservation on this date or invalid input. Please enter a valid date.  ##")
                print("#" * 95, "\n")
        except Exception:
            print("#" * 43)
            print("##  Invalid input. Enter integers only!  ##")
            print("#" * 43, "\n")

def get_slot_to_cancel(number_of_slots):
    while True:
        try:
            print("Enter '0' to cancel the operation.")
            if number_of_slots == 1:
                slot = int(input(f"Slot number to cancel [1]: "))
            else:
                slot = int(input(f"Slot number to cancel [1 - {number_of_slots}]: "))
            if 0 <= slot <= number_of_slots:
                return slot
            print("#" * 62)
            print("##  Invalid slot number. Please enter a valid slot number.  ##")
            print("#" * 62, "\n")
        except ValueError:
            print("#" * 43)
            print("##  Invalid input. Enter integers only!  ##")
            print("#" * 43, "\n")



















############################################
############ UPDATE RESERVATION ############
############################################
def display_reservation_information(reservation, date, is_update=True):
    if is_update:
        print("Details for the reservation you wish to update:")
    else:
        print("Confirm your reservation details:")
    print(f"[1] Date{':':>11}", date)
    print(f"[2] Slot Number{':':>4}", reservation[0][5])
    print(f"[3] Name{':':>11}", reservation[1])
    print(f"[4] Email{':':>10}", reservation[2])
    print(f"[5] Phone Number{':':>3}", reservation[3])
    print(f"[6] Pax{':':>12}", reservation[4])
    print()

def display_update_reservation(reservation_list):
    reservations = reservation_list[1:]

    print("-" * 83)
    print(f"|{'Reservations for ' + reservation_list[0]:^81}|")
    print("-" * 83)
    print(f"| Slot No. | Name               | Email                       | Phone No.   | Pax |")
    print("-" * 83)

    for i, reservation in enumerate(reservations, start=1):
        slot, name, email, phone, pax = reservation
        print(f"[{i:<1}] {slot:<6} | {name:<18} | {email:<27} | {phone:<11} | {pax:^3} |")
    print("-" * 83, "\n")

    return len(reservations)

def get_date_input_to_update(reservation_list):
    while True:
        try:
            date_list = get_date_list(reservation_list)
            input_date = input("Enter the reservation date you wish to update (YYYY-MM-DD): ").strip()
            if input_date in date_list:
                return input_date
            else:
                print("#" * 95)
                print("##  There might be no reservation on this date or invalid input. Please enter a valid date.  ##")
                print("#" * 95, "\n")
        except ValueError:
            print("#" * 43)
            print("##  Invalid input. Enter integers only!  ##")
            print("#" * 43, "\n")

def get_slot_to_update(number_of_slots):
    while True:
        try:
            print("Enter '0' to cancel the operation.")
            if number_of_slots == 1:
                slot = int(input(f"Slot number to update [1]: "))
            else:
                slot = int(input(f"Slot number to update [1 - {number_of_slots}]: "))
            if 0 <= slot <= number_of_slots:
                return slot
            else:
                print("#" * 62)
                print("##  Invalid slot number. Please enter a valid slot number.  ##")
                print("#" * 62, "\n")
        except ValueError:
            print("#" * 43)
            print("## Invalid input. Enter integers only!  ##")
            print("#" * 43, "\n")



def update(slot, reservation, reservation_slot_list, reservation_list):
    date_list = get_date_list(reservation_list)
    
    new_date = reservation[0]
    reservation_information = reservation_list[date_list.index(new_date)][slot]
    reservation_slot = reservation_list[date_list.index(new_date)][1:]
    slot_number = slot
    while True:
        try:
            display_reservation_information(reservation_information, new_date)
            #print("Enter the number of the detail you wish to update OR 'Y' to confirm changes OR 'N' to cancel: ")
            choice = input("Enter the number of the detail you wish to update OR 'Y' to confirm changes OR 'N' to cancel: ").strip().upper()
            if choice == "Y":
                print("#" * 78)
                print(f"##  Successfully update the reservation for {reservation_information[1]}, Slot {slot} on {reservation[0]}.  ##")
                print("#" * 78, "\n")
                break
            elif choice == "N":
                print("#" * 26)
                print("##  Changes cancelled.  ##")
                print("#" * 26, "\n")
                break
            elif 1 <= int(choice) <= 6:
                if int(choice) == 1:
                    clear_screen()
                    new_date = input("Enter new date (YYYY-MM-DD): ")
                    if validate_date_input(new_date):
                        if new_date == reservation[0]:
                            print("You have entered the same date as the original reservation.")
                        else:
                            if new_date not in date_list:
                                date_list.append(new_date)
                                reservation_list.append([new_date])
                            if len(reservation) == 2:
                                new_reservation_information_list = reservation[1]
                                if len(reservation_list[date_list.index(new_date)]) < 9:
                                    reservation_list.pop(date_list.index(reservation[0]))
                                    new_date_reservation_list = reservation_list[date_list.index(new_date) - 1]
                                    new_date_reservation_list.append(new_reservation_information_list)
                                    slot_number = len(new_date_reservation_list) - 1
                                else:
                                    print("#" * 40)
                                    print(f"##  No available slots on {new_date}  ##")
                                    print("#" * 40, "\n")
                                    new_date = reservation[0]
                            else:
                                new_reservation_information_list = reservation[slot]
                                if len(reservation_list[date_list.index(new_date)]) < 9:
                                    reservation.pop(slot)
                                    new_date_reservation_list = reservation_list[date_list.index(new_date)]
                                    new_date_reservation_list.append(new_reservation_information_list)
                                    slot_number = len(new_date_reservation_list) - 1
                                else:
                                    print("#" * 40)
                                    print(f"##  No available slots on {new_date}  ##")
                                    print("#" * 40, "\n")
                                    new_date = reservation[0]
                            #print(reservation_list)
                    elif not validate_date_input(new_date):
                        clear_screen()
                        print("#" * 48)
                        print("##  Invalid date. Please enter a valid date.  ##")
                        print("#" * 48)
                        new_date = reservation[0]
                elif int(choice) in range(2, 7):
                    clear_screen()
                    if int(choice) == 2:
                        entry = get_slot(check_available_slots(reservation_slot))
                        reservation_information[0] = entry
                    elif int(choice) == 3:
                        reservation_information[1] = get_name()
                    elif int(choice) == 4:
                        reservation_information[2] = get_email()
                    elif int(choice) == 5:
                        reservation_information[3] = get_phone_number()
                    elif int(choice) == 6:
                        reservation_information[4] = get_pax()
                    clear_screen()
            else:
                clear_screen()
                print("#" * 53)
                print("##  Invalid choice. Please enter a valid choice.  ##")
                print("#" * 53, "\n")
        except ValueError:
            clear_screen()
            print("#" * 57)
            print("##  Invalid input. Enter integers or 'Y' or 'N' only!  ##")
            print("#" * 57, "\n")

def validate_date_input(date_string):
    try:
        datetime.strptime(date_string, "%Y-%m-%d")
        return True
    except ValueError:
        return False
