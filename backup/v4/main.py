# CSC 1024 Python Final Assignment

import random
from utils import *
from datetime import datetime

# 6 functions

############################################
################# MAIN MENU ################
############################################
def menu():
    RESERVATION_FILE = "reservation_23026149.txt"
    reservation_list = get_organized_list(get_date_list(read_from_reservation_file(RESERVATION_FILE)), read_from_reservation_file(RESERVATION_FILE))

    while True:
        display_menu()
        option = get_menu_option()
        if option == 1:
            add_reservation(reservation_list)
        elif option == 2:
            cancel_reservation(reservation_list)
        elif option == 3:
            update_reservation(reservation_list)
        elif option == 4:
            display(reservation_list)
            press_enter_to_continue()
        elif option == 5:
            generate_recommendation()
            press_enter_to_continue()
        elif option == 6:
            write_reservation_data_to_file(reservation_list, RESERVATION_FILE)
            clear_screen()
            exit("Goodbye!")

############################################
################### NO 1 ###################
############################################
def add_reservation(reservation_list):
    trigger = True
    date_list = get_date_list(reservation_list)

    while True:
        if trigger:
            clear_screen()
        user_input = input("Enter the reservation date (YYYY-MM-DD): ").strip()
        if not validate_reservation_date(user_input):
            trigger = False
            continue
        date = datetime.strptime(user_input, "%Y-%m-%d").date()
        date = date.strftime("%Y-%m-%d")
        
        if date not in date_list:
            date_list.append(date)
            reservation_list.append([date])
        
        reservation = reservation_list[date_list.index(date)]
        if len(reservation) - 1 >= 8:
            print("#" * 80)
            print(f"##  There are no available slots for {date}. Please choose another date.  ##")
            print("#" * 80, "\n")
            trigger = False
            continue
        
        clear_screen()
        slots = reservation[1:]
        slot = get_slot(check_available_slots(slots))
        information_list = [
            slot,
            get_name(),
            get_email(),
            get_phone_number(),
            get_pax()
        ]
        clear_screen()
        display_reservation_information(information_list, date, False)

        if confirm_add_reservation():
            reservation.append(information_list)
            slot = information_list[0].replace("'", "").strip()
            clear_screen()
            print("#" * 74)
            print(f"##   Reservation for {slot} on {date} has been made successfully!   ##")
            print("#" * 74)
        else:
            clear_screen()
            print("#" * 31)
            print("##  Add reservation failed.  ##")
            print("#" * 31)                
        if not prompt_switch():
            break
        else:
            trigger = True

############################################
################### NO 2 ###################
############################################
def cancel_reservation(reservation_list):
    while True:
        clear_screen()
        input_date = get_date_input_to_cancel(reservation_list)
        clear_screen()
        for reservation in reservation_list:
            if input_date == reservation[0]:
                number_of_slots = display_cancel_reservation(reservation)
                slot = get_slot_to_cancel(number_of_slots)
                clear_screen()
                cancel_slot(input_date, slot, reservation_list)
        if not prompt_switch():
            break
        
############################################
################### NO 3 ###################
############################################
def update_reservation(reservation_list):
    while True:
        clear_screen()
        input_date = get_date_input_to_update(reservation_list)
        clear_screen()
        for reservation in reservation_list:
            if input_date == reservation[0]:
                reservation_slot_list = reservation[1:]
                number_of_slots = display_update_reservation(reservation)
                slot = get_slot_to_update(number_of_slots)
                clear_screen()
                update(slot, reservation, reservation_slot_list, reservation_list)
        if not prompt_switch():
            break

############################################
################### NO 4 ###################
############################################
def display(reservation_list):
    clear_screen()
    reservation_list.sort()
    for date, *reservations in reservation_list:
        print("-" * 81)
        print(f"|{' ' * 34}{date}{' ' * 35}|")
        print("-" * 81)
        for slot, name, email, phone_number, pax in reservations:
            print(f"| {slot:<6} | {name:<19} | {email:<28} | {phone_number:<11} | {pax} |")
        print("-" * 81)
        print()

############################################
################### NO 5 ###################
############################################       
def generate_recommendation():
    menuList = read_from_menu_file()
    clear_screen()
    print(" " * 20 + "#" * 26)
    print(" " * 20 + "##  Recommended dishes  ##")
    print(" " * 20 + "#" * 26, "\n")
    print("     -------------------------------------------------------------      ")
    print("    /                                                             \     ")
    print(f"   |       {random.choice(menuList):<50}      |    ")
    print("    \                                                             /     ")
    print("     -------------------------------------------------------------      ")
    print()

menu()
