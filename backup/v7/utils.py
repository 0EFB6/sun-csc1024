import os
from datetime import date,datetime, timedelta

# 30 Functions

############################################
################# Utilities ################
############################################

# Clear the screen based on the OS type and ptint a new line for better looking
def clear_screen():
     os.system("cls" if os.name == "nt" else "clear")
     print()

# Display the restaurant management system menu
def display_menu():
    clear_screen()
    print("Charming Thyme Trattoria Restaurant Management System")
    print("\n[1] Add Reservation(s)")
    print("[2] Cancel Reservation(s)")
    print("[3] Update/Edit Reservation(s)")
    print("[4] Display")
    print("[5] Generate Meal Recommendation")
    print("[6] Exit")
    print("-" * 36)
    print("| Enter a number (1-6) to continue |")
    print("-" * 36)

# Generate date list containing all the dates in the reservation list
def generate_date_list(reservation_list):
    return sorted(set(reservation[0] for reservation in reservation_list))

# Generate name list containing all the names in the reservation list
def generate_name_list(reservation_list):
    name_list = [reservation[1] for item in reservation_list for reservation in item[1:]]
    return name_list

# Generate organized list containing all the dates and reservations in the reservation list
# No storing of multiple date is allowed
def generate_organized_list(date_list, reservation_list):
    organized_list = []
    for date in date_list:
        reservation = [reservation[1:] for reservation in reservation_list if reservation[0] == date]
        organized_list.append([date] + reservation)
    return organized_list

# Get selection from user in main menu
# Validate and make sure input is an integer between 1 and 6
def get_menu_selection():
    while True:
        try:
            selection = int(input("Your choice: "))
            if 1 <= selection <= 6:
                return selection
            else:
                print("#" * 34)
                print("# Enter integer between 1 and 6! #")
                print("#" * 34)
        except ValueError:
            print("#" * 50)
            print("##  Invalid input. Please enter integers only!  ##")
            print("#" * 50)

def press_enter_to_continue():
    input("Press Enter to continue...")

# Prompt user to enter "Y" or "N" to continue current operation (add, cancel, update)
# or return to main menu
# Validate input and make sure input is either lowercase or uppercase "Y" or "N"
def get_prompt_switch():
    while True:
        print()
        switch = input("Enter 'Y' to continue perform current operation or 'N' to return to main menu: ").strip().upper()
        if switch == "Y":
            return True
        elif switch == "N":
            return False
        print("#" * 53)
        print("##  Invalid choice. Please enter 'Y' or 'N' only!  ##")
        print("#" * 53, "\n")

# Return a list of dishes from the menu file
def read_from_menu_file(file_name="menu.txt"):
    with open(file_name, "r") as file:
        return [menu.strip() for menu in file]

# Return a list of reservations with duplicating dates from the reservation file
def read_from_reservation_file(file_name="reservation.txt"):
    with open(file_name, "r") as file:
        reservation_list = [reservation.strip().split("|") for reservation in file if reservation.strip()]
    reservation_list.sort()
    return reservation_list

# Perform date validation by passing the date input from user as argument
def validate_date(user_input, mode="add"):
    try:
        # Convert user input(str) to date object
        input_date = datetime.strptime(user_input, "%Y-%m-%d").date()
        if len(user_input) != 10:
            raise ValueError
        if mode == "add":
            # Variable type of date object
            current_date = datetime.now().date()
            target_date = current_date + timedelta(days=8)

            # Deny user to enter date before current date
            if input_date >= target_date:
                return True
            else:
                print("#" * 78)
                print(f"##  The date you entered is invalid. Please enter a date after {(target_date - timedelta(days=1)).strftime('%Y-%m-%d')}.  ##")
                print("#" * 78, "\n")
                return False
        elif mode == "update":
            # Variable type of date object
            today = date.today()

            # Deny user to enter date before current date
            if input_date >= today:
                return True
            return False
    except ValueError:
        if mode == "add":
            print("#" * 74)
            print("##  Invalid date format. Please enter a date in the YYYY-MM-DD format.  ##")
            print("#" * 74, "\n")
        return False

# Write all changes in the main reservation list to the reservation text file
def write_reservation_data_to_file(reservation_list, file_name="reservation.txt"):
    formatted_reservations = [
        "|".join([date, slot, name, email, phone_number, pax])
        for date, *slots in reservation_list 
        for slot, name, email, phone_number, pax in slots
    ]
    with open(file_name, "w") as file:
        file.write('\n'.join(formatted_reservations))















############################################
################## SHARED ##################
############################################
# Check if there is available slots for a particular date and return a dictionary of available slots
# Original main reservation list is passed as argument when the mode is update
# Pure reservations is passed as argument when the mode is add and new_date is False
# ie. reservation_list = [[slot, name, email, phone_number, pax], [slot, name, email, phone_number, pax], ...]
def check_available_slots(reservation_list, new_date=False, mode=None, date=None, slot=None):
    if mode == "update":
        # Used for checking slot conflict when updating new date
        if slot:
            try:
                # Get reservations for the new date
                reservations = [item for item in reservation_list if item[0] == date]
                
                # Since reservations is a list of lists, we need to access it using [0][1:] to get all reservations made
                # Check if there is already 2 reservations made for the new date and slot
                count = sum(1 for item in reservations[0][1:] if isinstance(item, list) and item[0] == slot)
                return count >= 8
            except IndexError:
                return False
        # Used for checking available slots when updating new date
        else:
            return any(item[0] == date and len(item) == 33 for item in reservation_list)
    else:
        clear_screen()
        available_slots = {
            "Slot 1": 8,
            "Slot 2": 8,
            "Slot 3": 8,
            "Slot 4": 8
        }
        # Return default value since it is a new date (not existed in the reservation list & date list)
        if new_date:
            return available_slots
        
        # Return available slots for the date existed in the reservation list & date list
        for reservation in reservation_list:
            slot = reservation[0]
            if slot in available_slots:
                available_slots[slot] -= 1
        return available_slots

# Display a confirmation message before running the operation
# Mode is either "add" or "cancel" or a different message
# Perform date validation by only allowing user to enter "Y" or "N"
def confirm_prompt(mode="add"):
    # Variable for storing messages to be printed out & format width
    input_message = f"Confirm {mode} reservation? [Y/N]: "
    decline_message = f"##  {mode.capitalize()} reservation failed.  ##"
    width = 29 if mode == "cancel" else 31
    
    while True:
        choice = input(input_message).strip().upper()
        if choice == "Y":
            return True
        elif choice == "N":
            clear_screen()
            print("#" * width)
            print(decline_message)
            print("#" * width)
            return False
        else:
            print("#" * 42)
            print("##  Invalid choice. Enter Y or N only!  ##")
            print("#" * 42, "\n")

# Argument reservation_list contains one date and numbers of reservations made for that date
# Display all reservations made for that date
# Return the number of reservations made for that date
def display_reservations(reservation_list):
    divider = "-" * 83
    print(divider)
    print(f"|{'Reservations for ' + reservation_list[0]:^81}|")
    print(divider)
    print("| Slot No. | Name               | Email                       | Phone No.   | Pax |")
    print(divider)

    for i, reservation in enumerate(reservation_list[1:], start=1):
        slot, name, email, phone, pax = reservation
        print(f"[{i:<1}] {slot:<6} | {name:<18} | {email:<27} | {phone:<11} | {pax:^3} |")
    print(divider, "\n")

    return len(reservation_list) - 1

# Argument reservation contain a list of reservations information only, excluding date
# Display all reservations informations
# Mode is either "add" or "update" to be used for confirmation of adding reservation
# and updating reservation, not needed for cancel operation as it is replaced by
# another display function
# When printing slot number, access the number only, excluding the word "Slot"
def display_reservation_information(reservation, date, is_update=True):
    operation = "update" if is_update else "add"
    print(f"Details for the reservation you wish to {operation}:")
    print(f"[1] Date{':':>11}", date)
    print(f"[2] Slot Number{':':>4}", reservation[0][5])
    print(f"[3] Name{':':>11}", reservation[1])
    print(f"[4] Email{':':>10}", reservation[2])
    print(f"[5] Phone Number{':':>3}", reservation[3])
    print(f"[6] Pax{':':>12}", reservation[4])
    print()

# Get date input from user
# Validate the input and ignore all input which are not in the date list
# Used for cancellation of reservation and selecting reservation for updating
# Not used for updating to a new date from an existing date
# Strip leading and trailing whitespaces when getting input from user
# Return date as a string
def get_date_input(reservation_list, mode="update"):
    mode = "cancel" if mode == "cancel" else "update"
    while True:
        date_list = generate_date_list(reservation_list)
        input_date = input(f"Enter the reservation date you wish to {mode} (YYYY-MM-DD): ").strip()
        if input_date in date_list:
            return input_date
        else:
            print("#" * 95)
            print("##  There might be no reservation on this date or invalid input. Please enter a valid date.  ##")
            print("#" * 95, "\n")

# Get email input from user
# Perform email validation by only allowing user to enter a valid email
# Strip leading and trailing whitespaces when getting input from user
# Prompt user to enter email again if the email is invalid
# Return email as a string
def get_email():
    clear_screen()
    while True:
        email = input("Enter customer email: ").strip()
        if check_email(email):
            return email
        else:
            print("#" * 50)
            print("##  Invalid email. Please enter a valid email.  ##")
            print("#" * 50, "\n")

# Get name input from user
# Perform name validation by only allowing user to enter letters and spaces only,
#  no numbers or special characters
# Strip leading and trailing whitespaces when getting input from user
# Capitalize the input
# Prompt user to enter name again if the name is invalid
# Return name as a string
def get_name(name_list):
    clear_screen()
    while True:
        name = input("Enter customer name: ").strip().upper()
        if check_name(name) and name not in name_list:
            return name
        else:
            print("#" * 59)
            print("##  Invalid name. Please enter letters and spaces only.  ##")
            print("##  Only one reservation can be made per person!         ##")
            print("#" * 59, "\n")

# Get pax input from user
# Perform pax validation by only allowing user to enter 1 - 4 pax only
# Prompt user to enter pax again if the pax is invalid
# Return the pax as a string
def get_pax():
    clear_screen()
    while True:
        try:
            pax = int(input("Enter number of pax: "))
            if 1 <= pax <= 4:
                return str(pax)
            else:
                print("#" * 37)
                print("##  You may only enter 1 - 4 pax.  ##")
                print("#" * 37, "\n")
        except ValueError:
            print("#" * 43)
            print("##  Invalid input. Enter integers only!  ##")
            print("#" * 43, "\n")

# Get phone number input from user
# Perform phone number validation by only allowing user to enter 10 or 11 digits 
# Strip leading and trailing whitespaces when getting input from user
# Malaysian phone number(h/p) only
# Prompt user to enter phone number again if the phone number is invalid
# Return the phone number as a string
def get_phone_number():
    clear_screen()
    while True:
        phone = input("Enter customer phone number: ").strip()
        if phone.isdigit() and check_phone_number(phone):
            return phone
        else:
            print("#" * 64)
            print("##  Invalid phone number. Please enter 10 or 11 digits only.  ##")
            print("#" * 64, "\n")

# Get slot input from user, accept integers only
# Mode is either "add" or "update" or "update_slot_on_new_date" or "update_operation" or "cancel_operation"
# Prompt user to enter slot again if the slot is invalid
#
# When mode is "add" or "update", get slot input from user and validate the input
# Display available slots before getting input from user
# Return slot as an integer if mode is "update", return slot as a string if mode is "add
#
# When mode is "update_slot_on_new_date", get slot input from user and validate the input
# Slot range from 1 to the argument count
# Return slot as an integer
#
# When mode is "update_operation" or "cancel_operation", get slot input from user and validate the input
# Slot range from 0 to the argument number_of_slots
# 0 is used to cancel the operation
# Return slot as an integer
def get_slot(available_slots={}, mode="add", date=None, count=0, number_of_slots=0):
    if mode == "update_slot_on_new_date":
        while True:
            try:
                slot = int(input(f"Select a slot on {date}: "))
                if 1 <= slot <= count:
                    return slot
                else:
                    print("#" * 20)
                    print("##  Out of slot!  ##")
                    print("#" * 20, "\n")
            except ValueError:
                print("#" * 42)
                print("## Invalid input. Enter integers only!  ##")
                print("#" * 42, "\n")
    elif mode == "update_operation" or mode == "cancel_operation":
        while True:
            try:
                print("Enter '0' to cancel the operation.")
                if number_of_slots == 1:
                    slot = int(input(f"Slot number to {mode[:6]} [1]: "))
                else:
                    slot = int(input(f"Slot number to {mode[:6]} [1 - {number_of_slots}]: "))
                if 0 <= slot <= number_of_slots:
                    return slot
                print("#" * 62)
                print("##  Invalid slot number. Please enter a valid slot number.  ##")
                print("#" * 62, "\n")
            except ValueError:
                print("#" * 43)
                print("##  Invalid input. Enter integers only!  ##")
                print("#" * 43, "\n")
    elif mode =="add" or mode == "update":
        print("These are the available slots for the date you entered: ")
        i = 1
        ret = []
        for slot, count in available_slots.items():
            if count > 0:
                time, target_slot = get_slot_time(slot)
                print(f"[{i}] {slot}: {time}")
                ret.append(target_slot)
                i += 1
        if mode == "update":
            i = 34 - i
        while True:
            try:
                if i == 1:
                    return i
                if i == 2:
                    slot = int(input(f"Enter the slot of the reservation[1]: "))
                else:
                    slot = int(input(f"Enter the slot of the reservation[1 - {i - 1}]: "))
                if 1 <= slot < i:
                    if mode == "update":
                        return slot
                    return f"Slot {ret[slot - 1]}"
                else:
                    print("#" * 20)
                    print("##  Out of slot!  ##")
                    print("#" * 20, "\n")
            except ValueError:
                print("#" * 42)
                print("## Invalid input. Enter integers only!  ##")
                print("#" * 42, "\n")




















############################################
############## ADD RESERVATION #############
############################################
# Check email if it is valid by determining the existence of "@" and "."
# Make sure there is at least one character between "@" and ".", before "@" and after "."
def check_email(email):
    if "@" in email and "." in email:
        at_index = email.index("@")
        dot_index = email.index(".")
        if dot_index - at_index > 1  and at_index > 0 and dot_index < len(email) - 1:
            return True
    return False

# Make sure name does not contain any digits or special characters
def check_name(name):
    return name.replace(" ", "").isalpha()

# Check phone number if it is a valid Malaysia h/p phone numberby determining the length of the phone number
# and the prefix of the phone number
def check_phone_number(phone):
    return (len(phone) == 10 and phone.startswith("01") and not phone.startswith("011") and not phone.startswith("015")) \
        or (len(phone) == 11 and (phone.startswith("011") or phone.startswith("015")))

# Return time and slot_number as string based on the argument slot
# Act as a small sub function for get_slot()
def get_slot_time(slot):
    slot_times = {
        "Slot 1": "12:00 pm - 02:00 pm",
        "Slot 2": "02:00 pm - 04:00 pm",
        "Slot 3": "06:00 pm - 08:00 pm",
        "Slot 4": "08:00 pm - 10:00 pm"
    }
    slot_number = str(next((index + 1 for index, value in enumerate(slot_times) if value == slot), None))
    time = slot_times.get(slot, 'Unknown Time')
    return time, slot_number




















############################################
############ CANCEL RESERVATION ############
############################################
# Do nothing if slot is 0
# Otherwise, ask for confirmation
# If confirmed, remove the reservation from the reservation list, or else do nothing
def cancel_slot(input_date, slot, reservation_list):
    if slot == 0:
        print("#" * 29)
        print("##  Cancellation aborted.  ##")
        print("#" * 29, "\n")
        return
    
    confirmation = confirm_prompt(mode="cancel")
    if not confirmation:
        return
    
    # When the date matches the input date, remove the reservation from the reservation list
    # If there is only 1 reservation made for the date, remove both the date and the reservation
    # Or else, remove the reservation only
    for reservation in reservation_list:
        if input_date == reservation[0]:
            index = reservation_list.index(reservation) - 1
            if len(reservation_list[index + 1]) == 2:
                reservation_list.pop(index + 1)
            else:
                reservation_list[index + 1].pop(slot)
            print("#" * 77)
            print(f"##  Reservation for Slot {slot} on {input_date} has been cancelled successfully!  ##")
            print("#" * 77, "\n")
            return




















############################################
############ UPDATE RESERVATION ############
############################################
# When user choose to update date, check for available slots on the target date(new date)
def display_available_slot_on_new_date(date, reservation_list):
    available_slots = {
        "Slot 1": 8,
        "Slot 2": 8,
        "Slot 3": 8,
        "Slot 4": 8
    }
    slot_times = {
        "Slot 1": "12:00 pm - 02:00 pm",
        "Slot 2": "02:00 pm - 04:00 pm",
        "Slot 3": "06:00 pm - 08:00 pm",
        "Slot 4": "08:00 pm - 10:00 pm"
    }
    reservation = [item[1:] for item in reservation_list if item[0] == date]
    
    for item in reservation[0]:
        slot = item[0]
        if slot in available_slots:
            available_slots[slot] -= 1

    available_slots_count = sum(1 for count in available_slots.values() if count > 0)
    if available_slots_count > 0:
        i = 1
        ret = []
        for slot, count in available_slots.items():
            if count > 0:
                print(f"[{i}] {slot}: {slot_times[slot]}")
                ret.append(slot)
                i += 1
        
        if i > 1:
            slot = get_slot(None, mode="update_slot_on_new_date", date=date, count=i - 1)
            return ret[slot - 1]
    return 0

# Get user input on which information to update
# Return the selection as integer if its range is 1 to 6
# Return "Y" if user choose to confirm changes
# Return "N" if user choose to cancel changes
def get_selection_to_update():
    while True:
        try:
            choice = input("Enter the number of the detail you wish to update OR 'Y' to confirm changes OR 'N' to cancel: ").strip().upper()
            if choice == "Y":
                return choice
            elif choice == "N":
                print("#" * 26)
                print("##  Changes cancelled.  ##")
                print("#" * 26, "\n")
                return choice
            elif 1 <= int(choice) <= 6:
                return int(choice)
            else:
                clear_screen()
                print("#" * 53)
                print("##  Invalid choice. Please enter a valid choice.  ##")
                print("#" * 53, "\n")
        except ValueError:
            clear_screen()
            print("#" * 57)
            print("##  Invalid input. Enter integers or 'Y' or 'N' only!  ##")
            print("#" * 57, "\n")

# Get user input on the particular information to update
# Then, update the changes to a temporary list named ammeded_changes
# Don't update directly to the main reservation list as this will affect the original reservation
# even if user choose to cancel the changes
def update(slot, reservation, reservation_list, selection, date_list, amended_changes, reservation_slots, new_date):
    original_date = reservation[0]
    name_list = generate_name_list(reservation_list)

    # Date update
    # 1. Validate date
    # 2. Check input date against the original date
    # 3. Check for available slots on the new date (Totally no available slots)
    # 4. Check for available slots on the new date (Available slots found, prompt for slot 
    #    selection if no available slots is available for current slot)
    if int(selection) == 1:

        clear_screen()
        original_date = input("Enter new date (YYYY-MM-DD): ")
        
        if not validate_date(original_date, mode="update"):
            clear_screen()
            print("#" * 48)
            print("##  Invalid date. Please enter a valid date.  ##")
            print("#" * 48, "\n")

        elif original_date == reservation[0]:
            print("#" * 67)
            print("##  You have entered the same date as the original reservation.  ##")
            print("#" * 67, "\n")
        elif check_available_slots(reservation_list, new_date=False, mode="update", date=original_date, slot=None):
            print("#" * 41)
            print(f"##  No available slots on {original_date}.  ##")
            print("#" * 41, "\n")
        elif check_available_slots(reservation_list, new_date=False, mode="update", date=original_date, slot=amended_changes[0]):
            print("#" * 67)
            print(f"##  Slot conflict! No available slots on {original_date} for Slot {slot}.  ##")
            print("#" * 67, "\n")
            new_slot = display_available_slot_on_new_date(original_date, reservation_list)
            
            if new_slot:
                new_date = original_date
                reservation_slots[slot - 1][0] = amended_changes[0] = new_slot
        else:
            new_date = original_date

    elif int(selection) in range(2, 7):
        clear_screen()

        if int(selection) == 2:
            entry = None

            if new_date == original_date:
                entry = get_slot(check_available_slots(reservation_slots))
            elif new_date in date_list:
                reservation_slots_orig = reservation_list[date_list.index(new_date)][1:]
                reservation_slots = [item[:] for item in reservation_slots_orig]
                entry = get_slot(check_available_slots(reservation_slots))
            else:
                entry = get_slot(check_available_slots(None, True))
            reservation_slots[slot - 1][0] = amended_changes[0] = entry
            
        elif int(selection) == 3:
            amended_changes[1] = get_name(name_list)
        elif int(selection) == 4:
            amended_changes[2] = get_email()
        elif int(selection) == 5:
            amended_changes[3] = get_phone_number()
        elif int(selection) == 6:
            amended_changes[4] = get_pax()
        
        clear_screen()

    return new_date

# Write changes to reservation_list
# ammended_changes is a temperory list which contains changes to be made to the reservation
# Remove the original reservation from reservation_list first if the new date is different from the original date
# Then, append the new reservation to reservation_list depending on whether the new date exist in reservation_list or not
# If the new date is the same as the original date, update it directly
def write_changes(slot, date, amended_changes, reservation_list, new_date):
    if new_date:
        existing_reservation = reservation_list[date]
        if len(existing_reservation) == 2:
            reservation_list.remove(existing_reservation)
        elif len(existing_reservation) > 2:
            existing_reservation.pop(slot)
        
        new_date_reservation = next((item for item in reservation_list if item[0] == new_date), None)
        if new_date_reservation:
            new_date_reservation.append(amended_changes)
        else:
            reservation_list.append([new_date, amended_changes])
    else:
        reservation_list[date][slot] = amended_changes
