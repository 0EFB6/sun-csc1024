# CSC 1024 Python Final Assignment

import random
from utils import *
from datetime import datetime

# 6 functions

############################################
################# MAIN MENU ################
############################################
def menu():
    # Read reservation details from file and put it inside reservation list based on date
    reservation_file = "reservation_23026149.txt"
    reservation_list = generate_organized_list(generate_date_list(read_from_reservation_file(reservation_file)), read_from_reservation_file(reservation_file))

    # Main menu, will always be displayed until user exit
    while True:
        display_menu()
        option = get_menu_selection()
        if option == 1:
            add_reservation(reservation_list)
        elif option == 2:
            cancel_reservation(reservation_list)
        elif option == 3:
            update_reservation(reservation_list)
        elif option == 4:
            display(reservation_list)
            press_enter_to_continue()
        elif option == 5:
            generate_recommendation()
            press_enter_to_continue()
        elif option == 6:
            write_reservation_data_to_file(reservation_list, reservation_file)
            clear_screen()
            exit("Goodbye!")

############################################
################### NO 1 ###################
############################################
def add_reservation(reservation_list):
    trigger_clear_screen = True

    while True:
        reservation = []

        # Get date list from reservation_list
        date_list = generate_date_list(reservation_list)

        # Get name list from reservation_list to prevent multiple reservations made by same person
        name_list = generate_name_list(reservation_list)

        # Clear screen when needed
        if trigger_clear_screen:
            clear_screen()

        # Get date input from user
        date_input = input("Enter the reservation date (YYYY-MM-DD): ").strip()

        # Check if date is valid, else reprompt
        if not validate_date(date_input, mode="add"):
            trigger_clear_screen = False
            continue
        
        # Add date if not exist, then get slot input from user.
        # Check also if there are available slots for the date.
        is_new_date = False
        if date_input not in date_list:
            date_list.append(date_input)
            slot = get_slot(check_available_slots(None, True), mode="add")
            is_new_date = True
        else:
            reservation = reservation_list[date_list.index(date_input)][1:]
            if len(reservation) >= 32:
                print("#" * 80)
                print(f"##  There are no available slots for {date_input}. Please choose another date.  ##")
                print("#" * 80, "\n")
                trigger_clear_screen = False
                continue
            slot = get_slot(check_available_slots(reservation), mode="add")
        
        # Get other necessary information from user and store inside a temporary list
        # Then display the reservation information to user for confirmation
        information_list = [
            slot,
            get_name(name_list),
            get_email(),
            get_phone_number(),
            get_pax()
        ]
        clear_screen()
        display_reservation_information(information_list, date_input, is_update=False)

        # Prompt user for confirmation, if yes, append the information list to reservation list and display success message
        # Or else ignore and prompt user for multiple operations or exit
        if confirm_prompt(mode="add"):
            
            reservation_list.append([date_input, information_list]) if is_new_date else reservation_list[date_list.index(date_input)].append(information_list)
            clear_screen()
            print("#" * 74)
            print(f"##   Reservation for {slot} on {date_input} has been made successfully!   ##")
            print("#" * 74)

        # Prompt user for multiple operations or exit
        if not get_prompt_switch():
            break
        else:
            trigger_clear_screen = True

############################################
################### NO 2 ###################
############################################
def cancel_reservation(reservation_list):
    while True:
        # Clear screen & get date input from user and validate input
        clear_screen()
        input_date = get_date_input(reservation_list, mode="cancel")
        clear_screen()

        for reservation in reservation_list:
            if input_date == reservation[0]:

                # Display reservation information
                number_of_slots = display_reservations(reservation)

                # Get slot input to cancel from user
                slot = get_slot(available_slots=None, mode="cancel_operation", date=None, count=0, number_of_slots=number_of_slots)
                clear_screen()
                
                # Remove reservation from reservation list
                cancel_slot(input_date, slot, reservation_list)
        
        # Prompt user for multiple operations or exit
        if not get_prompt_switch():
            break
        
############################################
################### NO 3 ###################
############################################
def update_reservation(reservation_list):
    while True:
        # Clear screen & get date input from user and validate input
        clear_screen()
        input_date = get_date_input(reservation_list, mode="update")
        clear_screen()

        # Loop through reservation list and find the reservation with the input date
        for reservation in reservation_list:
            if input_date == reservation[0]:

                # Get list with all reservations on the input date excluding the date
                reservation_slot_list = reservation[1:]
                
                # Display all reservations available for update based on the input date
                display_reservations(reservation)

                # Get slot input to update from user, cancel operation if 0
                slot = get_slot(available_slots=None, mode="update_operation", date=None, count=0, number_of_slots=len(reservation_slot_list))
                if slot == 0:
                    break

                # Generate list full of date only which contains reservation for functions call usage & other usage
                clear_screen()
                date_list = generate_date_list(reservation_list)

                # Original reservation information according to slot entered by user before changes
                # For printing success message purposes
                original_information = reservation_list[date_list.index(input_date)][slot]

                # Reservation information that is subject to change after user choose to update certain information
                # Act as a temporary list to store changes before actually writing to the main reservation list
                amended_changes = reservation_list[date_list.index(input_date)][slot][:]

                # Original reservations information on original date excluding the date
                reservation_slots_orig = reservation_list[date_list.index(reservation[0])][1:]

                # Reservation information that is subject to change after user choose to update certain information
                # inside the while loop
                reservation_slots = [item[:] for item in reservation_slots_orig]

                # Variable to store original date, subject to change if user choose to change date
                new_date = reservation[0]

                while True:
                    # Display updated reservation information to user for confirmation
                    display_reservation_information(amended_changes, new_date, is_update=True)

                    # Get user input for information selection to update (N = No, Y = Yes, 1 = Date
                    # 2 = Name, 3 = Email, 4 = Phone Number, 5 = Pax, 6 = Date)
                    selection = get_selection_to_update()


                    if selection in ["N", "Y"]:
                        if selection == "Y":
                            # Write changes to the main reservation list
                            date_index = date_list.index(input_date)
                            write_changes(slot, date_index, amended_changes, reservation_list, new_date)

                            # Display success message
                            padding = (18 - len(original_information[1])) // 2
                            print("#" * 89)
                            print(f"##  Successfully update the reservation for {' ' * padding}{original_information[1]}{' ' * padding}, {original_information[0]} on {input_date}.  ##")
                            print("#" * 89, "\n")
                        break

                    # Update the relevant selection according to user input
                    new_date = update(slot, reservation, reservation_list, selection, date_list, amended_changes, reservation_slots, new_date)
        
        # Prompt user for multiple operations or exit
        if not get_prompt_switch():
            break

############################################
################### NO 4 ###################
############################################
def display(reservation_list):
    # Loop through reservation list and display reservation information based on date category
    clear_screen()
    for date, *reservations in reservation_list:
        print("-" * 81)
        print(f"|{' ' * 34}{date}{' ' * 35}|")
        print("-" * 81)
        for slot, name, email, phone_number, pax in reservations:
            print(f"| {slot:<6} | {name:<19} | {email:<28} | {phone_number:<11} | {pax} |")
        print("-" * 81)
        print()

############################################
################### NO 5 ###################
############################################       
def generate_recommendation():
    # Get menu list from menu file and display a random dish
    menu_list = read_from_menu_file("menuItems_23026149.txt")
    clear_screen()
    print(" " * 20 + "#" * 24)
    print(" " * 20 + "##  Recommended dish  ##")
    print(" " * 20 + "#" * 24, "\n")
    print("     -------------------------------------------------------------      ")
    print("    /                                                             \     ")
    print(f"   |       {random.choice(menu_list):<50}      |    ")
    print("    \                                                             /     ")
    print("     -------------------------------------------------------------      ")
    print()

def main():
    menu()

if __name__ == "__main__":
    main()
