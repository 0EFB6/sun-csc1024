# CSC 1024 Python Final Assignment
# By Group: Code Overflow
# Members: 23026149,
# Date: 2023-07-24
# Description: This program is a restaurant reservation system that allows
#              users to make, update and cancel reservations. It also
#              generates random recommendations of dishes from the menu.
# Files: python_23026149.py, menuItems_23026149.txt, reservation_23026149.txt
#        (menuItems_23026149.txt and reservation_23026149.txt are sample files
#        for testing purposes only.)
# Functions: menu(), add_reservation(), cancel_reservation(),
#            update_reservation(), display(), generate_recommendation(),
#            clear_screen(), display_menu(), generate_date_list(),
#            generate_name_list(), generate_organized_list(),
#            get_menu_selection(), press_enter_to_continue(),
#            get_prompt_switch(), read_from_menu_file(),
#            read_from_reservation_file(), validate_date(),
#            write_reservation_data_to_file(), check_available_slot(),
#            confirm_prompt(), display_reservations(),
#            display_reservation_information(), get_date_input(), get_email().
#            get_name(), get_pax(), get_phone_number(), get_slot()
#            check_email(), check_name(), check_phone_number(),
#            get_slot_time(), cancel_slot(),
#            display_available_slot_on_new_date(), get_selection_to_update(),
#            update(), write_changes(), main()
# Number of functions: 38
# Number of lines: 1287
#
# Codes are categorized into few sections and line number is stated:
# 1. MAIN MENU FUNCTION [48] (1 function)
# 2. ADD RESERVATION FUNCTION [81] (1 function)
# 3. CANCEL RESERVATION FUNCTION [181] (1 function)
# 4. UPDATE RESERVATION FUNCTION [212] (1 function)
# 5. DISPLAY RESERVATION FUNCTION [315] (1 function)
# 6. GENERATE RECOMMENDATION FUNCTION [338] (1 function)
# 7. UTILITIES [386] (12 functions)
# 8. SHARED FUNCTIONS [564] (10 functions)
# 9. ADD RESERVATION [941] (4 functions)
# 10. CANCEL RESERVATION [1000] (1 function)
# 11. UPDATE RESERVATION [1044] (4 functions)
# 12. MAIN FUNCTION [1273] (1 function)
# 13. RUN PROGRAM [1285]

import os
import random
from datetime import date, datetime, timedelta

# MAIN MENU FUNCTION: Handles the main menu and user interactions.


def menu():
    # Read reservation details from file and organize them based on date.
    res_file = "reservation_23026149.txt"
    date_list = generate_date_list(read_from_reservation_file(res_file))
    res_file_list = read_from_reservation_file(res_file)
    reservation_list = generate_organized_list(date_list, res_file_list)

    # Display the main menu options and process the user's choice by using if,
    # elif and else statement. Use While Loop to keep the program running
    # until the user chooses to exit.
    while True:
        display_menu()
        option = get_menu_selection()
        if option == 1:
            add_reservation(reservation_list)
        elif option == 2:
            cancel_reservation(reservation_list)
        elif option == 3:
            update_reservation(reservation_list)
        elif option == 4:
            display(reservation_list)
            press_enter_to_continue()
        elif option == 5:
            generate_recommendation()
            press_enter_to_continue()
        elif option == 6:
            write_reservation_data_to_file(reservation_list, res_file)
            clear_screen()
            exit("Goodbye!\n")

# ADD RESERVATION FUNCTION: Handles the process of adding
#                           a new reservation.


def add_reservation(reservation_list):
    # Initialize a trigger to clear the screen when needed.
    trigger_clear_screen = True

    # Use While Loop to collect reservation information from the user.
    while True:
        reservation = []

        # Get the list of existing reservation dates to easily
        # locate the index in reservation_list.
        date_list = generate_date_list(reservation_list)

        # Get the list of existing names to prevent multiple
        # reservations made by same person.
        name_list = generate_name_list(reservation_list)

        # Clear the screen when needed.
        if trigger_clear_screen:
            clear_screen()

        # Get the date input from the user, strip unexpected spaces.
        date_input_message = "Enter the reservation date (YYYY-MM-DD): "
        date_input = input(date_input_message).strip()

        # Validate the user's date input, reprompt if it's invalid.
        if not validate_date(mode="add", user_input=date_input):
            trigger_clear_screen = False
            continue

        # Check if the date is new or already exists in the
        # date list. Add the date to the date list if it's new.
        is_new_date = False
        if date_input not in date_list:
            date_list.append(date_input)
            # If the date is new, get the slot input from the user and
            # show all available slots to user. Argument passed to
            # check_available_slot() is None because there is no
            # existing reservation on the new date.
            avail_slot = check_available_slot(None, None, None, None, True)
            slot = get_slot(mode="add", available_slots=avail_slot)
            is_new_date = True
        else:
            # Locate the reservation in reservation_list based on
            # the date input.
            reservation = reservation_list[date_list.index(date_input)][1:]
            if len(reservation) >= 32:
                # If there are no available slots for the existing
                # date, reprompt the user for a new date.
                print("#" * 80)
                print(f"##  There are no available slots for {date_input}. \
Please choose another date.  ##")
                print("#" * 80, "\n")
                trigger_clear_screen = False
                continue
            # Get slot input from user and check the available slots.
            avail_slot = check_available_slot(None, reservation)
            slot = get_slot(mode="add", available_slots=avail_slot)

        # Get other necessary information from the user and store it
        # in a temporary list. Then display the reservation information
        # to the user for confirmation.
        info_list = [
            slot,
            get_name(name_list),
            get_email(),
            get_phone_number(),
            get_pax()
        ]
        clear_screen()
        display_reservation_information(info_list, date_input, False)

        # Sort the date list to make sure reservation list is organized.
        date_list.sort()

        # Prompt the user for confirmation, if yes, add the information
        # list to the reservation list and display success message.
        # If no, ignore reservations details entered earlier and prompt
        # the user for multiple operations or exit.
        if confirm_prompt(mode="add"):
            pos = date_list.index(date_input)
            if is_new_date:
                reservation_list.insert(pos, [date_input, info_list])
            else:
                reservation_list[date_list.index(date_input)].append(info_list)
            clear_screen()
            print("#" * 74)
            print(f"##   Reservation for {slot} on {date_input} has \
been made successfully!   ##")
            print("#" * 74)

        # Prompt user for multiple operations or exit
        if not get_prompt_switch():
            break
        else:
            trigger_clear_screen = True

# CANCEL RESERVATION FUNCTION: Allows users to cancel a
#                              reservation for a specific date and slot.


def cancel_reservation(reservation_list):
    while True:
        # Clear screen & get date input from user and validate input
        clear_screen()
        input_date = get_date_input("cancel", reservation_list)
        clear_screen()

        # Check if any reservation matches the input date
        for reservation in reservation_list:
            if input_date == reservation[0]:

                # Display reservation information for the selected date
                number_of_slots = display_reservations(reservation)

                # Get slot input to cancel from user
                slot = get_slot("cancel_operation", None, None, 0,
                                number_of_slots)
                clear_screen()

                # Remove reservation from the reservation list for the
                # selected date and slot
                cancel_slot(reservation_list, input_date, slot)

        # Prompt user for multiple operations or exit the looop
        if not get_prompt_switch():
            break

# UPDATE RESERVATION FUNCTION: Function to update a reservation in the
#                              reservation_list


def update_reservation(reservation_list):
    while True:
        # Clear the screen & get the date input from the user
        # and validate input
        clear_screen()
        input_date = get_date_input(mode="update",
                                    reservation_list=reservation_list)
        clear_screen()

        # Check if any reservation matches the input date
        for reservation in reservation_list:
            if input_date == reservation[0]:
                # Get a list with all reservations on the input date
                # excluding the date
                reservation_slot_list = reservation[1:]

                # Display reservation information for the selected date
                display_reservations(reservation)

                # Get the slot input to update from the user, cancel
                # operation if 0
                slot = get_slot(mode="update_operation", available_slots=None,
                                date=None, count=0,
                                no_of_slot=len(reservation_slot_list))
                if slot == 0:
                    break

                # Cleart the screen and generate a list with dates only
                # which contains reservation for functions call usage
                clear_screen()
                date_list = generate_date_list(reservation_list)

                # Original reservation information according to slot
                # entered by the user before changes
                # For printing the success message purposes
                index = date_list.index(input_date)
                original_information = reservation_list[index][slot]

                # Reservation information that is subject to change
                # after the user chooses to update certain information
                # Act as a temporary list to store changes before
                # actually writing to the main reservatin list
                index = date_list.index(input_date)
                amended_changes = reservation_list[index][slot][:]

                # Original reservations information on the original
                # date excluding the date
                index = date_list.index(reservation[0])
                reservation_slots_orig = reservation_list[index][1:]

                # Reservation information that is subject to change
                # after the user chooses to update certain information
                # inside the while loop
                reservation_slots = [item[:]
                                     for item in reservation_slots_orig]

                # Variable to store the original date, subject to
                # change if the user chooses to change the date
                new_date = reservation[0]

                while True:
                    # Display updated reservation information to the
                    # user for confirmation
                    display_reservation_information(amended_changes,
                                                    new_date, is_update=True)

                    # Get user input for information selection to
                    # update (N = No, Y = Yes, 1 = Date
                    # 2 = Name, 3 = Email, 4 = Phone Number, 5 = Pax, 6 = Date)
                    selection = get_selection_to_update()

                    if selection in ["N", "Y"]:
                        if selection == "Y":
                            # Write changes to the main reservation list
                            date_index = date_list.index(input_date)
                            write_changes(reservation_list, amended_changes,
                                          date_index, slot, new_date)

                            # Display the success message
                            sp = (18 - len(original_information[1])) // 2 * ' '
                            name = original_information[1]
                            slot = " " + original_information[0] + " "
                            date = " " + new_date
                            msg = "  Successfully update the reservation for "
                            print("#" * 89)
                            print(f"##{msg}{sp}{name}{sp},{slot}on{date}.  ##")
                            print("#" * 89, "\n")
                        break

                    # Update the relevant selection according to the
                    # user input
                    new_date = update(selection, reservation_list,
                                      amended_changes, slot, reservation,
                                      reservation_slots, new_date)

        # Prompt the user for multiple operations or exit
        if not get_prompt_switch():
            break

# DISPLAY RESERVATION FUNCTION: Function to display all reservation
#                               information


def display(reservation_list):
    clear_screen()

    # Sort the reservation list by slot number and then display
    # reservation information for each date by using for loop.
    sorted_reservation_list = [
        sorted(item, key=lambda x: x[0]) for item in reservation_list
    ]

    for date, *reservations in sorted_reservation_list:
        print("-" * 81)
        print(f"|{' ' * 34}{date}{' ' * 35}|")
        print("-" * 81)
        for slot, name, email, hp, pax in reservations:
            print(f"| {slot:<6} | {name:<19} | {email:<28} \
| {hp:<11} | {pax} |")
        print("-" * 81)
        print()

# GENERATE RECOMMENDATION FUNCTION: Function to generate
#                                   random recommendation of dishes


def generate_recommendation():
    # Get the menu list from the menu file and display a random
    # set of recommended dishes
    menu_list = read_from_menu_file("menuItems_23026149.txt")
    clear_screen()

    while True:
        try:
            count = int(input("How many recommended dishes do you want? "))

            # Check if the input count is within the valid range of
            # available dishes
            if count < 1 or count > len(menu_list):
                print("#" * 64)
                print(f"##  Please enter a number greater than 0 and \
lesser than {len(menu_list) + 1:<2}.  ##")
                print("#" * 64, "\n")
            else:
                break
        except ValueError:
            # Handle the case when the user enters a non-integer value
            print("#" * 53)
            print("##  Please enter a valid number (integers ONLY!).  ##")
            print("#" * 53, "\n")

    clear_screen()
    # Randomly select dishes from the menu list based on the number
    # requested by the user
    recommended_dishes = random.sample(menu_list, count)

    # Print the randomly recommended dishes in a
    # decorative box using for loop
    print(" " * 20 + "#" * 24)
    print(" " * 20 + "##  Recommended dish  ##")
    print(" " * 20 + "#" * 24, "\n")
    print(f"     {'-' * 61}      ")
    print(f"    / {' ' * 59} \\     ")
    for dish in recommended_dishes:
        print(f"   |       {dish:<50}      |    ")
    print(f"    \\ {' ' * 59} /     ")
    print(f"     {'-' * 61}      ")
    print()


# UTILITIES: Functions used across the program

# Clear the screen based on the OS type and print a new line for better
# readability


def clear_screen():
    os.system("cls" if os.name == "nt" else "clear")
    print()

# Display the restaurant management system menu


def display_menu():
    clear_screen()
    print("Charming Thyme Trattoria Restaurant Management System")
    print("\n[1] Add Reservation(s)")
    print("[2] Cancel Reservation(s)")
    print("[3] Update/Edit Reservation(s)")
    print("[4] Display")
    print("[5] Generate Meal Recommendation")
    print("[6] Exit")
    print("-" * 36)
    print("| Enter a number (1-6) to continue |")
    print("-" * 36)

# Generate a list of unique dates from the reservation list


def generate_date_list(reservation_list):
    return sorted(set(reservation[0] for reservation in reservation_list))

# Generate a list of unique names in the reservation list


def generate_name_list(reservation_list):
    name_list = [reservation[1] for item in reservation_list
                 for reservation in item[1:]]
    return name_list

# Organize the reservation_list into a list of lists grouped by dates
# No storing of multiple date is allowed


def generate_organized_list(date_list, reservation_list):
    organized_list = []
    for date in date_list:
        # Extract reservations for the current date and add to the
        # organized list
        reservation = [reservation[1:] for reservation in
                       reservation_list if reservation[0] == date]
        organized_list.append([date] + reservation)
    return organized_list

# Get selection from the user in the main menu
# Validate and ensure the input is an integer between 1 and 6


def get_menu_selection():
    while True:
        try:
            # Get the user's selection from the main menu
            selection = int(input("Your choice: "))
            # Ensure the input is between 1 and 6
            if 1 <= selection <= 6:
                return selection
            else:
                print("#" * 34)
                print("# Enter integer between 1 and 6! #")
                print("#" * 34)
        except ValueError:
            msg = "Invalid input. Please enter integers only!"
            print("#" * 50)
            print(f"##  {msg}  ##")
            print("#" * 50)

# Pause and prompt the user to press Enter to continue


def press_enter_to_continue():
    input("Press Enter to continue...")

# Prompt the user to enter 'Y' or 'N' to continue the current
# operation (add, cancel, update) or return to the main menu
# Validate the input and ensure it is either lowercase or uppercase
# 'Y' or 'N'


def get_prompt_switch():
    while True:
        switch = input("\nEnter 'Y' to continue perform current operation\
or 'N' to return to main menu: ").strip().upper()
        if switch == "Y":
            return True
        elif switch == "N":
            return False
        print("#" * 53)
        print("##  Invalid choice. Please enter 'Y' or 'N' only!  ##")
        print("#" * 53, "\n")

# Return a list of dishes from the menu file


def read_from_menu_file(file_name="menu.txt"):
    try:
        with open(file_name, "r") as file:
            return [menu.strip() for menu in file]
    except FileNotFoundError:
        # Print error message if the menu file is not found
        print("#" * 46)
        print("##  Menu file not found! Please try again.  ##")
        print("#" * 46, "\n")
        exit()

# Return a list of reservations with duplicate dates from the
# reservation file


def read_from_reservation_file(file_name="reservation.txt"):
    try:
        with open(file_name, "r") as file:
            reservation_list = [reservation.strip().split("|") for reservation
                                in file if reservation.strip()]
        reservation_list.sort()
        return reservation_list
    except FileNotFoundError:
        # Print error message if the reservation file is not found
        print("#" * 53)
        print("##  Reservation file not found! Please try again.  ##")
        print("#" * 53, "\n")
        exit()

# Perform date validation by passing date input from user as argument


def validate_date(mode="add", user_input=""):
    try:
        # Convert user input(str) to date object
        input_date = datetime.strptime(user_input, "%Y-%m-%d").date()

        # Check if the user input has the correct format (YYYY-MM-DD)
        if len(user_input) != 10:
            raise ValueError

        if mode == "add":
            # Variable type of date object
            # Get the current date and calculate the target date (8 days ahead)
            current_date = datetime.now().date()
            target_date = current_date + timedelta(days=5)

            # Deny the user from entering a date before the current date
            if input_date >= target_date:
                return True
            else:
                # Print an error message if the date is invalid
                print("#" * 78)
                print(f"##  The date you entered is invalid. \
Please enter a date after \
{(target_date - timedelta(days=1)).strftime('%Y-%m-%d')}.  ##")
                print("#" * 78, "\n")
                return False
        elif mode == "update":
            # Variable type of date object
            # Get the current date
            today = date.today()

            # Deny user to enter date before current date
            if input_date >= today:
                return True
            return False
    except ValueError:
        if mode == "add":
            # Print an error message for invalid date format
            print("#" * 74)
            print("##  Invalid date format. \
Please enter a date in the YYYY-MM-DD format.  ##")
            print("#" * 74, "\n")
        return False

# Write all changes in the main reservation list to reservation text file


def write_reservation_data_to_file(reservation_list, file_name="res.txt"):
    # Format the reservation data as a string and write it to the file
    formatted_reservations = [
        "|".join([date, slot, name, email, phone_number, pax])
        for date, *slots in reservation_list
        for slot, name, email, phone_number, pax in slots
    ]
    with open(file_name, "w") as file:
        file.write('\n'.join(formatted_reservations))

# SHARED FUNCTIONS: Functions used by multiple operations


"""
Check if there are available slots for a particular date and return a
dictionary of available slots.

Parameters:
    mode (str): The mode of operation - "add", "update", etc.
    res_list (list): The main reservation list containing existing
                     reservations. Pure reservations is passed as argument
                     when the mode is 'add' and new_date is False
    date (str): The date for which to check available slots
                (only used in "update" mode).
    slot (str): The specific slot to check availability for
                (only used in "update" mode).
    new_date (bool): Flag indicating if it's a new date (not
                     existed in the reservation list & date list).

Returns:
    bool or dict: If mode is "update", returns True if the given slot
                  has reached maximum reservations, otherwise False.
                  If mode is "add", returns a dictionary with available
                  slots and their remaining capacity.
"""


def check_available_slot(mode, res_list, date=None, slot=None, new_date=False):
    if mode == "update":
        if slot:
            try:
                # Get reservations for the new date
                reservations = [item for item in res_list
                                if item[0] == date]

                # Since reservations is a list of lists, we need to access
                # it using [0][1:] to get all reservations made
                # Check if there is already 8 reservations made for the
                # new date and slot
                count = sum(1 for item in reservations[0][1:] if
                            isinstance(item, list) and item[0] == slot)
                return count >= 8
            except IndexError:
                return False
        else:
            return any(item[0] == date and len(item) == 33 for
                       item in res_list)
    else:
        clear_screen()
        available_slots = {
            "Slot 1": 8,
            "Slot 2": 8,
            "Slot 3": 8,
            "Slot 4": 8
        }
        # Return default value since it is a new date (not existed in the
        # reservation list & date list)
        if new_date:
            return available_slots

        # Return available slots for the date existed in the reservation
        # list & date list
        for reservation in res_list:
            slot = reservation[0]
            if slot in available_slots:
                available_slots[slot] -= 1
        return available_slots


"""
Display a confirmation message before running the operation.
Perform validation by only allowing user to enter "Y" or "N"

Parameters:
    mode (str): The mode of operation - "add", "cancel", etc.

Returns:
    bool: True if the user confirms the operation, False if the user declines.
"""


def confirm_prompt(mode="add"):
    # Variable for storing messages to be printed out & format width
    input_message = f"Confirm {mode} reservation? [Y/N]: "
    decline_message = f"##  {mode.capitalize()} reservation failed.  ##"
    width = 34 if mode == "cancel" else 31

    while True:
        choice = input(input_message).strip().upper()
        if choice == "Y":
            return True
        elif choice == "N":
            clear_screen()
            print("#" * width)
            print(decline_message)
            print("#" * width)
            return False
        else:
            print("#" * 42)
            print("##  Invalid choice. Enter Y or N only!  ##")
            print("#" * 42, "\n")


"""
Display all reservations made for a specific date.

Parameters:
    reservation_list (list): A list containing one date and numbers
    of reservations made for that date.

Returns:
    int: The number of reservations made for that date.
"""


def display_reservations(reservation_list):
    divider = "-" * 86
    print(divider)
    print(f"|{'Reservations for ' + reservation_list[0]:^84}|")
    print(divider)
    print("| Slot No.  | Name                | Email\
                        | Phone No.   | Pax |")
    print(divider)

    for i, reservation in enumerate(reservation_list[1:], start=1):
        slot, name, email, phone, pax = reservation
        print(f"[{i:>2}] {slot:<6} | {name:<19} | {email:<28} | {phone:<11} \
| {pax:^3} |")
    print(divider, "\n")

    return len(reservation_list) - 1


"""
Display detailed information for a reservation.

Parameters:
    reservation (list): A list containing reservation information
                        (excluding the date).
    date (str): The date of the reservation.
    is_update (bool): Flag indicating if the operation is for updating a
                    reservation (default is True).
"""


def display_reservation_information(reservation, date, is_update=True):
    operation = "update" if is_update else "add"
    print(f"Details for the reservation you wish to {operation}:")
    print(f"[1] Date{':':>11}", date)
    # When printing slot number, access the number only, excluding
    # the word "Slot"
    print(f"[2] Slot Number{':':>4}", reservation[0][5])
    print(f"[3] Name{':':>11}", reservation[1])
    print(f"[4] Email{':':>10}", reservation[2])
    print(f"[5] Phone Number{':':>3}", reservation[3])
    print(f"[6] Pax{':':>12}", reservation[4])
    print()


# Get date input from user
# Validate the input and ensure the entered date is in the reservation list
# Used for cancelling or updating a reservation, not for selecting a new date
# Strip leading and trailing whitespaces when getting input from the user
# Return date as a string


def get_date_input(mode="update", reservation_list=[]):
    # Ensure the mode is either "cancel" or "update"
    mode = "cancel" if mode == "cancel" else "update"
    while True:
        # Generate the list of valid dates from the reservation list
        date_list = generate_date_list(reservation_list)

        # Get the input date from the user and remove any leading/trailing
        # whitespaces
        input_date = input(f"Enter the reservation date you wish to \
{mode} (YYYY-MM-DD): ").strip()

        # Check if the entered date is in the list of valid dates
        if input_date in date_list:
            return input_date
        else:
            # Notify the user of an invalid input and prompt for a valid date
            print("#" * 95)
            print("##  There might be no reservation on this date or invalid \
input. Please enter a valid date.  ##")
            print("#" * 95, "\n")


# Get email input from the user
# Perform email validation by only allowing the user to enter a valid email
# format
# Strip leading and trailing whitespaces when getting input from the user
# Prompt the user to enter the email again if the email is invalid
# Return email as a string


def get_email():
    clear_screen()
    while True:
        email = input("Enter customer email: ").strip()
        if check_email(email):
            return email
        else:
            # Notify the user of an invalid email format and prompt for a
            # valid email
            print("#" * 55)
            print("##  Invalid email. Please enter a valid email.       ##")
            print("##  Email length should be less than 29 characters.  ##")
            print("#" * 55, "\n")


# Get name input from the user
# Perform name validation by only allowing the user to enter letters and
# spaces, no numbers or special characters
# Strip leading and trailing whitespaces when getting input from the user
# Capitalize the input name
# Prompt the user to enter the name again if it is invalid or already in
# the reservation list
# Return name as a string


def get_name(name_list):
    clear_screen()
    while True:
        name = input("Enter customer name: ").strip().upper()
        if check_name(name) and name not in name_list and len(name) <= 19:
            return name
        else:
            # Notify the user of an invalid name or duplicate reservation and
            # prompt for a valid name
            print("#" * 59)
            print("##  Invalid name. Please enter letters and spaces only.\
 ##")
            print("##  Only one reservation can be made per person!        \
 ##")
            print("##  Email length should be less than 20 characters.     \
 ##")
            print("#" * 59, "\n")


# Get pax input from the user
# Perform pax validation by only allowing the user to enter a value between
# 1 and 4
# Prompt the user to enter pax again if the input is invalid
# Return the pax as a string


def get_pax():
    clear_screen()
    while True:
        try:
            pax = int(input("Enter number of pax: "))
            if 1 <= pax <= 4:
                return str(pax)
            else:
                # Notify the user of an invalid pax value and prompt for a
                # valid pax number
                print("#" * 37)
                print("##  You may only enter 1 - 4 pax.  ##")
                print("#" * 37, "\n")
        except ValueError:
            # Notify the user of an invalid input and prompt for a valid
            # integer
            print("#" * 43)
            print("##  Invalid input. Enter integers only!  ##")
            print("#" * 43, "\n")


# Function to get a valid phone number from the user.
# The phone number must be 10 or 11 digits and should be for a Malaysian
# phone (h/p).
# Strips leading and trailing whitespaces from the user input.
# If the entered phone number is invalid, the user is prompted to enter again.
# Returns the validated phone number as a string.


def get_phone_number():
    clear_screen()
    while True:
        phone = input("Enter customer phone number: ").strip()
        if phone.isdigit() and check_phone_number(phone):
            return phone
        else:
            print("#" * 64)
            print("##  Invalid phone number. Please enter 10 or 11 digits \
only.  ##")
            print("#" * 64, "\n")


# Function to get a valid slot number from user based on the mode specified.
# Mode can be "add" or "update" or "update_slot_on_new_date" or
# "update_operation" or "cancel_operation".
#
# When mode is "add" or "update", the available slots are displayed, and the
# user must enter a valid slot number.
# Returns the slot as an integer if mode is "update", and as a string
# if mode is "add".
#
# When mode is "update_slot_on_new_date", the user selects a slot on a given
# date, and the slot number is returned as an integer.
#
# When mode is "update_operation" or "cancel_operation", the user selects a
# slot from a range of slots.
# The user can enter '0' to cancel the operation. Returns slot as an integer.


def get_slot(mode="add", available_slots={}, date=None, count=0, no_of_slot=0):
    if mode == "update_slot_on_new_date":
        while True:
            try:
                slot = int(input(f"Select a slot on {date}: "))
                if 1 <= slot <= count:
                    return slot
                else:
                    print("#" * 20)
                    print("##  Out of slot!  ##")
                    print("#" * 20, "\n")
            except ValueError:
                print("#" * 42)
                print("## Invalid input. Enter integers only!  ##")
                print("#" * 42, "\n")
    elif mode == "update_operation" or mode == "cancel_operation":
        while True:
            try:
                print("Enter '0' to cancel the operation.")
                if no_of_slot == 1:
                    slot = int(input(f"Slot number to {mode[:6]} [1]: "))
                else:
                    slot = int(input(f"Slot number to {mode[:6]} \
[1 - {no_of_slot}]: "))
                if 0 <= slot <= no_of_slot:
                    return slot
                print("#" * 62)
                print("##  Invalid slot number. Please enter a valid slot \
number.  ##")
                print("#" * 62, "\n")
            except ValueError:
                print("#" * 43)
                print("##  Invalid input. Enter integers only!  ##")
                print("#" * 43, "\n")
    elif mode == "add" or mode == "update":
        print("These are the available slots for the date you entered: ")
        i = 1
        ret = []
        for slot, count in available_slots.items():
            if count > 0:
                time, target_slot = get_slot_time(slot)
                print(f"[{i}] {slot}: {time}")
                ret.append(target_slot)
                i += 1
        if mode == "update":
            i = 34 - i
        while True:
            try:
                if i == 1:
                    return i
                if i == 2:
                    slot = int(input(f"Enter the slot of the reservation[1]\
: "))
                else:
                    slot = int(input(f"Enter the slot of the reservation[1 \
- {i - 1}]: "))
                if 1 <= slot < i:
                    if mode == "update":
                        return slot
                    return f"Slot {ret[slot - 1]}"
                else:
                    print("#" * 20)
                    print("##  Out of slot!  ##")
                    print("#" * 20, "\n")
            except ValueError:
                print("#" * 42)
                print("## Invalid input. Enter integers only!  ##")
                print("#" * 42, "\n")


# ADD RESERVATION: Functions specifically use by the add reservation function

# Check if the provided email is valid.
# A valid email must contain "@" and "." symbols,
# with at least one character before "@" and after ".",
# and the overall length should not exceed 28 characters.


def check_email(email):
    if "@" in email and "." in email:
        at_index = email.index("@")
        dot_index = email.index(".")
        if dot_index - at_index > 1 and at_index > 0 and \
                dot_index < len(email) - 1 and len(email) <= 28:
            return True
    return False


# Check if the provided name is valid.
# A valid name should not contain any digits or special characters.


def check_name(name):
    return name.replace(" ", "").isalpha()


# Check if the provided phone number is a valid Malaysian mobile phone number.
# A valid phone number must be 10 digits long and start with "01",
# or 11 digits long and start with "011" or "015".


def check_phone_number(phone):
    return (len(phone) == 10 and phone.startswith("01") and not
            phone.startswith("011") and not phone.startswith("015")) \
            or (len(phone) == 11 and (phone.startswith("011") or
                                      phone.startswith("015")))


# Return the time and slot number as a string based on the provided slot.
# This function acts as a small sub-function for get_slot()


def get_slot_time(slot):
    slot_times = {
        "Slot 1": "12:00 pm - 02:00 pm",
        "Slot 2": "02:00 pm - 04:00 pm",
        "Slot 3": "06:00 pm - 08:00 pm",
        "Slot 4": "08:00 pm - 10:00 pm"
    }

    #  Find the slot number corresponding to the provided slot name.
    slot_number = str(next((index + 1 for index, value in
                            enumerate(slot_times) if value == slot), None))

    # Get the time for the provided slot.
    time = slot_times.get(slot, 'Unknown Time')
    return time, slot_number


# CANCEL RESERVATION: Functions specifically use by the cancel reservation
#                     function

# Function to cancel a reservation slot
# If the slot is already empty (0), the cancellation is aborted
# If the user confirms the cancellation, the reservation is removed from the
# reservation list
# If there's only one reservation for the date, the date itself is removed
# from the list
# Otherwise, only the specific reservation for the given slot is removed


def cancel_slot(reservation_list, input_date, slot):
    if slot == 0:
        print("#" * 29)
        print("##  Cancellation aborted.  ##")
        print("#" * 29, "\n")
        return

    # Ask for user confirmation before proceeding with the cancellation
    confirmation = confirm_prompt(mode="cancel")
    if not confirmation:
        return

    # Find the reservation matching the input date
    for reservation in reservation_list:
        if input_date == reservation[0]:
            index = reservation_list.index(reservation) - 1

            # Check if the date has only one reservation
            if len(reservation_list[index + 1]) == 2:
                # Remove both the date and the reservation
                reservation_list.pop(index + 1)
            else:
                # Remove only the specific reservation for the given slot
                reservation_list[index + 1].pop(slot)

            print("#" * 77)
            print(f"##  Reservation for Slot {slot} on {input_date} has \
been cancelled successfully!  ##")
            print("#" * 77, "\n")
            return


# UPDATE RESERVATION: Functions specifically use by the update reservation

# Function to display available time slots on a given date after checking
# against reservations.
# Parameters:
#   - reservation_list: List of reservations with date and slot information.
#   - date: The target date for which to check available slots.
# Returns:
#   - The selected slot, or 0 if no available slots.


def display_available_slot_on_new_date(reservation_list, date):
    # Define available slots and their corresponding time intervals.
    available_slots = {
        "Slot 1": 8,
        "Slot 2": 8,
        "Slot 3": 8,
        "Slot 4": 8
    }
    slot_times = {
        "Slot 1": "12:00 pm - 02:00 pm",
        "Slot 2": "02:00 pm - 04:00 pm",
        "Slot 3": "06:00 pm - 08:00 pm",
        "Slot 4": "08:00 pm - 10:00 pm"
    }

    # Filter reservations for the target date.
    reservation = [item[1:] for item in reservation_list if item[0] == date]

    # Update available slot counts based on existing reservations.
    for item in reservation[0]:
        slot = item[0]
        if slot in available_slots:
            available_slots[slot] -= 1

    # Count the number of available slots.
    available_slots_count = sum(1 for count in available_slots.values()
                                if count > 0)

    # Display and return the available slots for the user to choose from.
    if available_slots_count > 0:
        i = 1
        ret = []
        for slot, count in available_slots.items():
            if count > 0:
                print(f"[{i}] {slot}: {slot_times[slot]}")
                ret.append(slot)
                i += 1

        # If there are available slots, prompt the user to choose one.
        if i > 1:
            slot = get_slot(mode="update_slot_on_new_date",
                            available_slots=None, date=date, count=i - 1)
            return ret[slot - 1]
    return 0


# Function to get user input for selecting the information to update.
# Returns:
#   - An integer representing the selected detail (1 to 6).
#   - "Y" if the user chooses to confirm changes.
#   - "N" if the user chooses to cancel changes.


def get_selection_to_update():
    while True:
        try:
            choice = input("Enter the number of the detail you wish to \
update OR 'Y' to confirm changes OR 'N' to cancel: ").strip().upper()
            if choice == "Y":
                return choice
            elif choice == "N":
                print("#" * 26)
                print("##  Changes cancelled.  ##")
                print("#" * 26, "\n")
                return choice
            elif 1 <= int(choice) <= 6:
                return int(choice)
            else:
                clear_screen()
                print("#" * 53)
                print("##  Invalid choice. Please enter a valid choice.  ##")
                print("#" * 53, "\n")
        except ValueError:
            clear_screen()
            print("#" * 57)
            print("##  Invalid input. Enter integers or 'Y' or 'N' only!  ##")
            print("#" * 57, "\n")


# Get user input on the particular information to update
# Then, update the changes to a temporary list named amended_changes
# Don't update directly to the main reservation list as this will affect
# the original reservation even if the user chooses to cancel the changes

def update(selection, reservation_list, amended_changes, slot,
           reservations_with_date, reservations_without_date, new_date):
    # Store the original date from the reservation with a date
    original_date = reservations_with_date[0]

    # Generate separate lists for names and dates from the reservation_list
    name_list = generate_name_list(reservation_list)
    date_list = generate_date_list(reservation_list)

    # Date update
    # 1. Validate the date provided by the user.
    # 2. Check if the input date is the same as the original date.
    # 3. Check for available slots on the new date (if there are no
    #    available slots).
    # 4. If the slot is already booked on the new date, prompt the user
    #    to choose another available slot.
    if int(selection) == 1:
        clear_screen()
        original_date = input("Enter new date (YYYY-MM-DD): ")

        if not validate_date(mode="update", user_input=original_date):
            clear_screen()
            print("#" * 48)
            print("##  Invalid date. Please enter a valid date.  ##")
            print("#" * 48, "\n")

        elif original_date == reservations_with_date[0]:
            print("#" * 67)
            print("##  You have entered the same date as the original \
reservation.  ##")
            print("#" * 67, "\n")
        elif check_available_slot("update", reservation_list,
                                  date=original_date):
            print("#" * 41)
            print(f"##  No available slots on {original_date}.  ##")
            print("#" * 41, "\n")
        elif check_available_slot("update", reservation_list,
                                  date=original_date, slot=amended_changes[0]):
            print("#" * 67)
            print(f"##  Slot conflict! No available slots on {original_date} \
for Slot {slot}.  ##")
            print("#" * 67, "\n")
            new_slot = display_available_slot_on_new_date(reservation_list,
                                                          original_date)

            if new_slot:
                new_date = original_date
                reservations_without_date[slot - 1][0] = new_slot
                amended_changes[0] = new_slot
        else:
            new_date = original_date
    # Handling other updates (name, email, phone number, pax)
    elif int(selection) in range(2, 7):
        clear_screen()

        # For updating the slot, check the available slots depending on the
        # date before getting input from user.
        if int(selection) == 2:
            entry = None

            if new_date == original_date:
                entry = get_slot(mode="add",
                                 available_slots=check_available_slot(
                                    None, reservations_without_date))
            elif new_date in date_list:
                reservation_slots_orig = reservation_list[
                    date_list.index(new_date)][1:]
                reservation_slots = [item[:] for item in
                                     reservation_slots_orig]
                entry = get_slot(mode="add",
                                 available_slots=check_available_slot(
                                    None, reservation_slots))
            else:
                entry = get_slot(mode="add",
                                 available_slots=check_available_slot(
                                    None, None, date=None, slot=None,
                                    new_date=True))
            reservations_without_date[slot - 1][0] = amended_changes[0] = entry
        # For other fields (name, email, phone number, pax), directly get the
        # updated value from the user.
        elif int(selection) == 3:
            amended_changes[1] = get_name(name_list)
        elif int(selection) == 4:
            amended_changes[2] = get_email()
        elif int(selection) == 5:
            amended_changes[3] = get_phone_number()
        elif int(selection) == 6:
            amended_changes[4] = get_pax()

        clear_screen()

    return new_date


# Function to write changes to the reservation_list
# amended_changes is a temporary list that contains changes to be made to the
# reservation
#
# If the new_date is provided, we'll update the reservation to the new date and
# time (slot)
#
# If the new_date is None, we'll update the existing reservation for the given
# date and slot


def write_changes(reservation_list, amended_changes, date, slot, new_date):
    if new_date:
        # Get the existing reservation for the original date
        existing_reservation = reservation_list[date]

        # If the existing reservation has only 2 elements, remove it completely
        if len(existing_reservation) == 2:
            reservation_list.remove(existing_reservation)
        # If the existing reservation has more than two elements, remove
        # the specific slot
        elif len(existing_reservation) > 2:
            existing_reservation.pop(slot)

        # Check if there's already a reservation for the new date
        new_date_reservation = next((item for item in reservation_list if
                                     item[0] == new_date), None)

        # If the new date reservation exists, append the amended changes to it
        if new_date_reservation:
            new_date_reservation.append(amended_changes)
        # If the new date reservation doesn't exist, create a new entry for it
        else:
            reservation_list.append([new_date, amended_changes])
    else:
        # Update the existing reservation according to the given date and
        # slot with the amended changes
        reservation_list[date][slot] = amended_changes


# MAIN FUNCTION: Define the main function to start the program.


def main():
    # Call the menu function to display the program's menu options.
    menu()

# The following block of code ensures that the main function is
# executed only when this script is run directly,
# not when it's imported as a module into another script.


if __name__ == "__main__":
    main()  # Start the main function and execute the program.
